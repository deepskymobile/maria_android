package com.deepskymobile.marya;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.UnknownHostException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

/**
 * Helper activity for executing POST and GET server requests
 * 
 * @author Denis Zakupnev
 * 
 */
public class ServerWorker {
	private static HttpClient client;
	
	public ServerWorker() {
		client = new DefaultHttpClient();
	}

	public static String getServerResponse(Context context, String url) {
		if (!isNetworkAvailable(context)) {
			Log.d(Constants.LOG_APPNAME, "Network not available");
			try {
				Toast.makeText(context, R.string.toast_nonetwork,
						Toast.LENGTH_LONG).show();
			} catch (Exception d) {
				Log.d(Constants.LOG_APPNAME, "Can`t display toast message");
			}
			return Constants.NO_INTERNET;
		}

		StringBuilder builder = new StringBuilder();
		//client = new DefaultHttpClient();
		HttpGet httpGet = null;
		httpGet = new HttpGet(url);
		httpGet.setHeader("Content-type", "application/json");

		try {
			HttpResponse response = client.execute(httpGet);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			Log.d(Constants.LOG_APPNAME,
					"Status code: " + Integer.toString(statusCode) + " "
							+ statusLine.getReasonPhrase());
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(Constants.LOG_APPNAME,
						"Download failed (getServerResponse)");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			return Constants.NO_INTERNET;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return builder.toString();
	}

	public static String postServerResponse(Context context, String url,
			String data) {
		if (!isNetworkAvailable(context)) {
			Log.d(Constants.LOG_APPNAME, "Network not available");
			try {
				Toast.makeText(context, R.string.toast_nonetwork,
						Toast.LENGTH_LONG).show();
			} catch (Exception d) {
				Log.d(Constants.LOG_APPNAME, "Can`t display toast message");
			}
			return Constants.NO_INTERNET;
		}

		StringEntity se = null;
		try {
			se = new StringEntity(data, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		StringBuilder builder = new StringBuilder();
		//client = new DefaultHttpClient();
		HttpPost httpPost = null;
		httpPost = new HttpPost(url);
		httpPost.setHeader("Content-type", "application/json");
		httpPost.setEntity(se);

		try {
			HttpResponse response = client.execute(httpPost);
			StatusLine statusLine = response.getStatusLine();
			int statusCode = statusLine.getStatusCode();
			Log.d(Constants.LOG_APPNAME,
					"Status code: " + Integer.toString(statusCode) + " "
							+ statusLine.getReasonPhrase());
			if (statusCode == 200) {
				HttpEntity entity = response.getEntity();
				InputStream content = entity.getContent();
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(content));
				String line;
				while ((line = reader.readLine()) != null) {
					builder.append(line);
				}
			} else {
				Log.e(Constants.LOG_APPNAME,
						"Download failed (postServerResponse)");
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			return Constants.NO_INTERNET;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return builder.toString();
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}
}
