package com.maria;

import java.util.UUID;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.maria.db.DatabaseHelper;
import com.maria.net.RequestsManager;
import com.maria.net.core.AbstractHttpLoader;

public class App extends Application {

	private static App instance;
	private static Db db;
	private static SharedPreferences settings;

	private RequestsManager requestsManager;
	public String uuid;

	@Override
	public void onCreate() {
		super.onCreate();
		settings = getSharedPreferences(Consts.SETTINGS_NAME, Context.MODE_PRIVATE);
		instance = this;
		db = new Db(getApplicationContext());
		loadUrls();
		requestsManager = new RequestsManager(getApplicationContext());
		requestsManager.register();

		uuid = settings.getString("UUID", null);
		if (uuid == null) {
			uuid = generateUUID();
			Editor edit = settings.edit();
			edit.putString("UUID", uuid);
			edit.commit();
		}
		AbstractHttpLoader.readCookies(getApplicationContext());
	}

	public static App getInstance() {
		return instance;
	}

	public static DatabaseHelper getHelper() {
		return db.getHelper();
	}

	public static void loadUrls() {
	}

	public boolean isRememberAuth() {
		return settings.getBoolean("remember", false);
	}

	/**
	 * RFC UUID generation
	 */
	public String generateUUID() {
		UUID uuid = UUID.randomUUID();
		StringBuilder str = new StringBuilder(uuid.toString());
		int index = str.indexOf("-");
		while (index > 0) {
			str.deleteCharAt(index);
			index = str.indexOf("-");
		}
		return str.toString();
	}

}
