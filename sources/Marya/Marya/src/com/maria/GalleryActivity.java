package com.maria;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.maria.R;
import com.droidful.flinggallery.FlingGallery;
import com.maria.model.Product;
import com.maria.model.ProductColor;
import com.maria.util.ImageFetcher;
import com.maria.util.UIUtils;

import de.greenrobot.event.EventBus;

/**
 * Activity for displaying product images
 * 
 * @author Fedor Kazakov
 * 
 */
public class GalleryActivity extends FragmentActivity {

	private int position;
	private FlingGallery mGalleryView;
	private TextView count;
	private Product product;
	private ImageFetcher mImageFetcher;
	private TextView name;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gallery);
		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);
		product = getIntent().getParcelableExtra("product");
		boolean imagesNotColors = getIntent().getBooleanExtra("images", true);
		Adapter adapter;
		if (imagesNotColors) {
			adapter = showImagesGallery();
		} else {
			adapter = showColorsGallery();
		}

		View nextBtn = findViewById(R.id.button_next_photo);
		View prevBtn = findViewById(R.id.button_prev_photo);
		nextBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mGalleryView.moveNext();
			}
		});

		prevBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				mGalleryView.movePrevious();
			}
		});
		((LinearLayout) findViewById(R.id.gallery_container)).addView(mGalleryView);
		count = (TextView) findViewById(R.id.tvCount);
		name = (TextView) findViewById(R.id.tvText);
		if (savedInstanceState != null && savedInstanceState.containsKey("current")) {
			position = savedInstanceState.getInt("current");
		}
		mGalleryView.setAdapter(adapter, position);
	}

	private Adapter showImagesGallery() {
		final String[] images;
		if (product.images != null && product.images.length > 0) {
			images = product.images;
		} else {
			images = new String[1];
			if (UIUtils.isValidImageUrl(product.big_image))
				images[0] = product.big_image;
			else if (UIUtils.isValidImageUrl(product.thumbnail_url))
				images[0] = product.thumbnail_url;
			else {
				finish();
			}
		}
		if (images == null)
			finish();
		mGalleryView = (FlingGallery) new FlingGallery(this);
		mGalleryView.setOnItemSelectedListener(new FlingGallery.OnItemSelectedListener() {
			@Override
			public void onItemSelected(FlingGallery galleryView, View itemView, int position, long id) {
				GalleryActivity.this.position = position;
				count.setText(getResources().getString(R.string.photo_count, position + 1, images.length));
				name.setVisibility(View.INVISIBLE);
				name.setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,
						LayoutParams.WRAP_CONTENT, 0.48f));
			}
		});
		
		if (images.length < 2)
			mGalleryView.setIsGalleryCircular(false);
		
		return new ColorAdapter(this, mImageFetcher, images);
	}

	private Adapter showColorsGallery() {
		final ProductColor[] colors = product.getColors();
		mGalleryView = (FlingGallery) new FlingGallery(this);
		mGalleryView.setOnItemSelectedListener(new FlingGallery.OnItemSelectedListener() {
			@Override
			public void onItemSelected(FlingGallery galleryView, View itemView, int position, long id) {
				GalleryActivity.this.position = position;
				count.setText(getResources().getString(R.string.photo_count, position + 1, colors.length));
				name.setText(colors[position].name);
			}
		});
		return new GalleryAdapter(this, mImageFetcher, colors);
	}

	@Override
	public void onPause() {
		super.onPause();
		mImageFetcher.flushCache();
		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("current", position);
	}

	private int tapDeltaX = 0;
	private int tapDeltaY = 0;

	@Override
	public boolean onTouchEvent(MotionEvent e) {
		if (e.getAction() == MotionEvent.ACTION_DOWN) {
			tapDeltaX = (int) e.getX();
			tapDeltaY = (int) e.getY();
		} else if (e.getAction() == MotionEvent.ACTION_UP || e.getAction() == MotionEvent.ACTION_OUTSIDE) {
			tapDeltaX = (int) (tapDeltaX - e.getX());
			tapDeltaY = (int) (tapDeltaY - e.getY());
			int absDelta = Math.max(Math.abs(tapDeltaX), Math.abs(tapDeltaY));

			if (absDelta < 10) {
				View view = findViewById(R.id.bottom_bar);
				View shadow = findViewById(R.id.shadow);
				if (view != null) {
					if (view.getVisibility() == View.VISIBLE) {
						view.setVisibility(View.GONE);
						shadow.setVisibility(View.GONE);
					} else {
						view.setVisibility(View.VISIBLE);
						shadow.setVisibility(View.VISIBLE);
					}
				}
			}
		}
		return mGalleryView.onGalleryTouchEvent(e);
	}
}
