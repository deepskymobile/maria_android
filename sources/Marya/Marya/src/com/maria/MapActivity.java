package com.maria;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.maria.R;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.maria.model.Error;
import com.maria.model.Response;
import com.maria.model.Shop;
import com.maria.model.Shops;
import com.maria.model.ShopsVersion;
import com.maria.net.RequestsManager;
import com.maria.net.core.events.OnLoaderFinishedEvent;
import com.maria.util.UIUtils;

import de.greenrobot.event.EventBus;

/**
 * Map activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class MapActivity extends android.support.v4.app.FragmentActivity implements LocationListener {
	public GoogleMap mMap;

	private Button mBtnList;
	private Button mBtnNear;

	private LocationManager locationManager;
	private Location location;
	private String provider;

	private Shop[] shops;
	private static final LatLng MOSCOW = new LatLng(55.754352, 37.623061);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);

		mBtnList = (Button) findViewById(R.id.btn_shops_list);
		mBtnNear = (Button) findViewById(R.id.btn_shops_near);

		mBtnList.setOnClickListener(oclListener);
		mBtnNear.setOnClickListener(oclListener);

		try {
			MapsInitializer.initialize(this);
		} catch (GooglePlayServicesNotAvailableException e) {
			e.printStackTrace();
		}

		// Get the location manager
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		// Define the criteria how to select the location provider -> use
		// default
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		location = locationManager.getLastKnownLocation(provider);

		// Initialize the location fields
		if (location != null) {
			Log.d(Constants.LOG_APPNAME, "Provider " + provider + " has been selected.");

		} else {
			Log.d(Constants.LOG_APPNAME, "Error: No provider selected");
		}
		UIUtils.startNewLoader(Consts.REQ_SHOP_VERSION, new Bundle(), null, getSupportLoaderManager());

	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_SHOPS.hashCode()) {
			Response<Shops> data = (Response<Shops>) event.getData();
			Error errData = data.getError();
			boolean error = data == null || errData != null;
			if (!error) {
				setUpMapIfNeeded();
				Shops shops = data.getData();
				addShops(shops);
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		} else if (event.getId() == Consts.REQ_SHOP_VERSION.hashCode()) {
			Response<ShopsVersion> data = (Response<ShopsVersion>) event.getData();
			Error errData = data.getError();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				boolean hasNewVer = hasNewerMapVersion(data.getData().updated);
				Bundle args = new Bundle();
				if (!hasNewVer) {
					args.putBoolean(RequestsManager.EXTRA_CACHE_FIRST, true);
				}
				UIUtils.startNewLoader(Consts.REQ_SHOPS, args, null, getSupportLoaderManager());
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		}
	}

	private boolean hasNewerMapVersion(String newTimestamp) {
		final String propName = "mapversion";
		SharedPreferences preferences = getApplicationContext().getSharedPreferences(Consts.SETTINGS_NAME,
				Context.MODE_PRIVATE);
		String mapVersion = preferences.getString(propName, "");
		if (mapVersion.equals(newTimestamp)) {
			return false;
		} else {
			preferences.edit().putString(propName, newTimestamp).apply();
			return true;
		}
	}

	private void addShops(final Shops shops) {
		this.shops = shops.shops;
		if (shops.shops == null)
			return;
		final BitmapDescriptor bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.drawable.shop_pin);
		final float archorU = 0.37f;
		final float archorV = 1.0f;
		for (int i = 0; i < shops.shops.length; i++) {
			Shop currentShop = shops.shops[i];
			String snippet = currentShop.getSnippet();
			// Adds marker with shop on the map
			MarkerOptions marker = new MarkerOptions()
					.position(new LatLng(currentShop.latitude, currentShop.longitude)).title(currentShop.name)
					.icon(bitmapDescriptor).anchor(archorU, archorV).snippet(snippet);
			mMap.addMarker(marker);
		}
		mMap.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

			@Override
			public void onInfoWindowClick(Marker marker) {
				Shop item = null;
				for (int i = 0; i < shops.shops.length; i++) {
					Shop currentShop = shops.shops[i];
					if (sameShop(currentShop, marker.getPosition())) {
						item = currentShop;
						break;
					}
				}
				Intent i = new Intent(MapActivity.this, ShopDescriptionActivity.class);
				i.putExtra("shop", item);
				startActivity(i);
			}
		});

		mMap.setInfoWindowAdapter(new InfoWindowAdapter() {

			@Override
			public View getInfoWindow(Marker marker) {
				View result = getLayoutInflater().inflate(R.layout.custom_info_window, null);
				TextView titleUi = ((TextView) result.findViewById(R.id.title));
				TextView snippetUi = ((TextView) result.findViewById(R.id.snippet));
				titleUi.setText(marker.getTitle());
				snippetUi.setText(marker.getSnippet());
				return result;
			}

			@Override
			public View getInfoContents(Marker marker) {
				View result = getLayoutInflater().inflate(R.layout.custom_info_window, null);
				return result;
			}
		});
	}

	protected boolean sameShop(Shop currentShop, LatLng position) {
		return Math.abs(currentShop.latitude - position.latitude) <= 0.000001
				&& Math.abs(currentShop.longitude - position.longitude) <= 0.000001;
	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_shops_list:
				// Open activity with shops list
				Intent i = new Intent(getApplicationContext(), ShopsListActivity.class);
				i.putExtra("shops", shops);
				if (location != null) {
					i.putExtra("location", location);
				}
				startActivity(i);
				break;
			case R.id.btn_shops_near:
				// Centers map on user location
				LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
				break;
			}
		}
	};

	@Override
	protected void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);

		setUpMapIfNeeded();

		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 10, this);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 10, this);
	}

	private void setUpMapIfNeeded() {
		if (mMap == null) {
			mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
			if (mMap != null) {
				mMap.getUiSettings().setZoomControlsEnabled(true);
				mMap.setMyLocationEnabled(true);
				if (location != null) {
					mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
							new LatLng(location.getLatitude(), location.getLongitude()), 15));

				} else {
					mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(MOSCOW, 12));
				}
			}
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		EventBus.getDefault().unregister(this);
		locationManager.removeUpdates(this);
	}

	boolean temp = true;

	@Override
	public void onLocationChanged(Location location) {
		this.location = location;
		if (temp) {
			temp = false;
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
					new LatLng(location.getLatitude(), location.getLongitude()), 15));
		}
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
	}

	@Override
	public void onProviderEnabled(String provider) {
	}

	@Override
	public void onProviderDisabled(String provider) {
	}

}
