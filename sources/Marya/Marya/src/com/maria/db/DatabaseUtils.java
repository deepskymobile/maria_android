/**
 * 
 */
package com.maria.db;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * DatabaseUtils
 * 
 * @author Valentin Telegin
 */
public class DatabaseUtils {
    
    public static <E> List<E> convertCollectionToList(Collection<E> collection) {
        List<E> list = null;
        if (collection != null && collection.size() > 0) {
            list = new ArrayList<E>();
            for (E e : collection) {
                list.add(e);
            }
        }
        return list;
    }
}
