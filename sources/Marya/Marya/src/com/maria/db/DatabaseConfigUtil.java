package com.maria.db;


import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;
import com.maria.net.core.Etag;
import com.maria.net.core.HttpResponseCache;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
	public static final Class<?>[] classes = new Class[] { Etag.class, HttpResponseCache.class };

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	}
}