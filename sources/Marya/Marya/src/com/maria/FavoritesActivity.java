package com.maria;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.maria.R;
import com.maria.favorites.FavoritesManagerImpl;
import com.maria.model.Product;
import com.maria.util.ImageFetcher;
import com.megatron.widget.RetriableProgressBar;
import com.megatron.widget.RetriableProgressBar.ProgressState;

/**
 * Activity for handling favorites
 * 
 * @author Fedor Kazakov
 * 
 */
public class FavoritesActivity extends FragmentActivity implements OnItemClickListener {

	private ImageFetcher mImageFetcher;
	private List<Product> products;
	private FavAdapter adapter;
	private ListView listView;
	private FavoritesManagerImpl favMgr;
	private RetriableProgressBar progressPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_favorites);
		listView = (ListView) findViewById(android.R.id.list);
		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);
		favMgr = FavoritesManagerImpl.getInstance(getApplicationContext());
	}

	@Override
	public void onResume() {
		super.onResume();
		products = favMgr.getAll();
		fillContent(products);
	}

	@Override
	public void onPause() {
		super.onPause();
		mImageFetcher.flushCache();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}

	private void fillContent(List<Product> products) {
		if (products.size() == 0) {
			progressPane.setState(ProgressState.EMPTY);
			findViewById(R.id.subcontent).setVisibility(View.GONE);
			findViewById(android.R.id.empty).setVisibility(View.VISIBLE);
			return;
		} else {
			progressPane.setState(ProgressState.SUCCESS);
			findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
			findViewById(android.R.id.empty).setVisibility(View.GONE);
		}
		adapter = new FavAdapter(this, mImageFetcher, products, removeClickListener);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(this);
	}

	private OnClickListener removeClickListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			Product item = (Product) v.getTag();
			FavoritesManagerImpl favMgr = FavoritesManagerImpl.getInstance(FavoritesActivity.this);
			favMgr.remove(item);
			products = favMgr.getAll();
			fillContent(products);
			Toast.makeText(FavoritesActivity.this, R.string.removed_to_favorites, Toast.LENGTH_SHORT).show();
		}
	};

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Product item = adapter.getItem(position);
		Intent i = new Intent(this, ProductActivity.class);
		i.putExtra(Consts.KEY_PARAM_ID, item.id);
		startActivity(i);
	}
}
