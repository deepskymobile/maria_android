/**
 * 
 */
package com.maria;


import android.content.Context;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.maria.db.DatabaseHelper;

/**
 * Db
 * 
 * @author Fedor Kazakov
 */
public class Db {
    private volatile DatabaseHelper helper;
    private volatile boolean created = false;

    public Db(Context context) {
        if (helper == null) {
            helper = getHelperInternal(context);
            created = true;
        }
    }
    
    /**
     * Get a helper for this action.
     */
    public DatabaseHelper getHelper() {
        if (helper == null) {
            if (!created) {
                throw new IllegalStateException("A call has not been made to onCreate() yet so the helper is null");
            } else {
                throw new IllegalStateException("Helper is null for some unknown reason");
            }
        } else {
            return helper;
        }
    }

    /**
     * Get a connection source for this action.
     */
    public ConnectionSource getConnectionSource() {
        return getHelper().getConnectionSource();
    }

    /**
     * This is called internally by the class to populate the helper object
     * instance. This should not be called directly by client code unless you
     * know what you are doing. Use {@link #getHelper()} to get a helper
     * instance. If you are managing your own helper creation, override this
     * method to supply this activity with a helper instance.
     * <p>
     * <b> NOTE: </b> If you override this method, you most likely will need to override the
     * {@link #releaseHelper(OrmLiteSqliteOpenHelper)} method as well.
     * </p>
     */
    protected DatabaseHelper getHelperInternal(Context context) {
        DatabaseHelper newHelper = new DatabaseHelper(context);
        return newHelper;
    }

}
