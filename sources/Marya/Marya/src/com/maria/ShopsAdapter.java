package com.maria;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maria.R;
import com.maria.model.Shop;

public class ShopsAdapter extends ArrayAdapter<Shop> {

	private Location location;
	private LayoutInflater inflater;
	private Shop[] shops;
	private String meterStr;
	private String kilometerStr;

	public ShopsAdapter(Context context, Shop[] shops, Location location) {
		super(context, R.id.name, new ArrayList<Shop>());
		this.shops = shops;
		this.location = location;
		inflater = LayoutInflater.from(getContext());
		meterStr = context.getString(R.string.geo_distance_unit);
		kilometerStr = context.getString(R.string.geo_distance_kilo_unit);
		for (Shop shop : shops) {
			add(shop);
		}
	}

	static class ViewHolder {
		TextView title;
		TextView description;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.shop_item, parent, false);

			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.name);
			holder.description = (TextView) convertView.findViewById(R.id.description);
			convertView.setTag(holder);
		}
		holder = (ViewHolder) convertView.getTag();
		Shop item = getItem(position);
		holder.title.setText(item.name);
		if (location != null) {
			float[] resultArray = new float[3];
			Location.distanceBetween(location.getLatitude(), location.getLongitude(), item.latitude, item.longitude,
					resultArray);
			holder.description.setText(item.getSnippet() + " (" + formatDist(resultArray[0]) + ")");
		} else {
			holder.description.setText(item.getSnippet());
		}

		return convertView;
	}

	public void sortByName() {
		Arrays.sort(shops, new Comparator<Shop>() {
			@Override
			public int compare(Shop lhs, Shop rhs) {
				return lhs.name.compareTo(rhs.name);
			}
		});
		clear();
		for (Shop item : shops) {
			add(item);
		}
		notifyDataSetChanged();
	}

	public String formatDist(double meters) {
		if (meters < 1000) {
			return ((int) meters) + meterStr;
		} else if (meters < 10000) {
			return formatDec(meters / 1000f, 1) + kilometerStr;
		} else {
			return ((int) (meters / 1000f)) + kilometerStr;
		}
	}

	static String formatDec(double val, int dec) {
		int factor = (int) Math.pow(10, dec);

		int front = (int) (val);
		int back = (int) Math.abs(val * (factor)) % factor;

		return front + "." + back;
	}

	public void sortByDistance() {
		if (location == null) {
			sortByName();
			return;
		}
		final double myLat = location.getLatitude();
		final double myLong = location.getLongitude();
		Arrays.sort(shops, new Comparator<Shop>() {
			@Override
			public int compare(Shop lhs, Shop rhs) {
				float[] resultArray = new float[3];
				Location.distanceBetween(myLat, myLong, lhs.latitude, lhs.longitude, resultArray);
				float dist1 = resultArray[0];
				Location.distanceBetween(myLat, myLong, rhs.latitude, rhs.longitude, resultArray);
				float dist2 = resultArray[0];
				return Double.valueOf(dist1).compareTo(Double.valueOf(dist2));
			}
		});
		clear();
		for (Shop item : shops) {
			add(item);
		}
		notifyDataSetChanged();
	}

}
