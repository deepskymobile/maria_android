package com.maria;

import com.maria.R;

import android.app.Activity;
import android.os.Bundle;

/**
 * About company activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class AboutcompanyActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aboutcompany);
	}

}
