package com.maria;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.maria.R;
import com.google.gson.Gson;
import com.maria.model.Error;
import com.maria.model.RemindReqData;
import com.maria.model.Response;
import com.maria.model.SimpleResp;
import com.maria.net.RequestsManager;
import com.maria.net.core.events.OnLoaderFinishedEvent;
import com.maria.util.UIUtils;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * Restore password activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class RestorePasswordActivity extends FragmentActivity {

	private EditText mEtRestoreEmail;
	private Button mBtnRestoreEmailSend;

	DialogFragment completionDialog;

	private RetriableProgressBar progressPane;

	private ViewSwitcher switcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_restore_password);

		mEtRestoreEmail = (EditText) findViewById(R.id.et_restore_email);
		mBtnRestoreEmailSend = (Button) findViewById(R.id.btn_restore_email_send);

		mBtnRestoreEmailSend.setOnClickListener(oclListener);

		completionDialog = new CustomAlertDialog();

		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		progressPane.setVisibility(View.INVISIBLE);
		switcher = (ViewSwitcher) findViewById(R.id.switcher);
	}

	@Override
	public void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
		progressPane.startEventListening();
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_restore_email_send:
				RemindReqData rd = new RemindReqData();
				rd.login = UIUtils.textOrError(mEtRestoreEmail, R.string.err_login_empty);
				if (rd.login == null) {
					return;
				}
				Bundle args = new Bundle();
				args.putString(RequestsManager.EXTRA_BODY, new Gson().toJson(rd));
				progressPane.setVisibility(View.VISIBLE);
				UIUtils.startNewLoader(Consts.REQ_REMIND, args, progressPane, getSupportLoaderManager());
				break;
			}
		}
	};

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_REMIND.hashCode()) {
			progressPane.setVisibility(View.INVISIBLE);
			switcher.setDisplayedChild(0);
			Response<SimpleResp> data = (Response<SimpleResp>) event.getData();
			Error errData = data.getError();
			boolean error = Response.isErrorResponse(data);
			switcher.setDisplayedChild(error ? 0 : 1);
			if (!error) {
				Toast.makeText(this, R.string.remind_ok, Toast.LENGTH_LONG).show();
				finish();
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		}
	}

}
