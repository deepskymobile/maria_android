package com.maria;

import com.maria.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;

/**
 * Helper DialogFragment for displaying "operation completed" alert
 * 
 * @author Fedor Kazakov
 * 
 */
public class CustomAlertDialog extends DialogFragment implements
		OnClickListener {

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
				.setPositiveButton(R.string.ok, this).setMessage(
						R.string.message_operation_completed);
		return adb.create();
	}

	@Override
	public void onClick(DialogInterface dialog, int which) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		super.onDismiss(dialog);
		Log.d(Constants.LOG_APPNAME, "CustomAlertDialog: onDismiss");
	}

	@Override
	public void onCancel(DialogInterface dialog) {
		super.onCancel(dialog);
		Log.d(Constants.LOG_APPNAME, "CustomAlertDialog: onCancel");
	}

}
