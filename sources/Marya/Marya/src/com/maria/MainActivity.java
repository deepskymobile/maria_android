package com.maria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.maria.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

/**
 * Main menu activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class MainActivity extends Activity {

	private ListView lvMainButtons;

	private View mBtnAbout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		lvMainButtons = (ListView) findViewById(R.id.lv_main_buttons);

		mBtnAbout = findViewById(R.id.btn_about);
		mBtnAbout.setOnClickListener(oclListener);

		createListItems();

	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_about:
				Intent i = new Intent(MainActivity.this, AboutProgramActivity.class);
				startActivity(i);
				break;
			}
		}
	};

	/**
	 * Creates list items
	 * 
	 */
	public void createListItems() {
		String[] texts = { getResources().getString(R.string.list_catalog),
				getResources().getString(R.string.list_nearshops), getResources().getString(R.string.list_feedback),
				getResources().getString(R.string.list_aboutcompany) };
		int[] imageIds = { R.drawable.ic_catalogue, R.drawable.ic_kitchens, R.drawable.ic_contacts, R.drawable.ic_about };

		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(texts.length);
		Map<String, Object> m;
		for (int i = 0; i < texts.length; i++) {
			m = new HashMap<String, Object>();
			m.put(Constants.ATTRIBUTE_NAME_TEXT, texts[i]);
			m.put(Constants.ATTRIBUTE_NAME_IMAGE, imageIds[i]);
			data.add(m);
		}

		String[] from = { Constants.ATTRIBUTE_NAME_TEXT, Constants.ATTRIBUTE_NAME_IMAGE };
		int[] to = { R.id.tvText, R.id.ivImg };

		// Creates adapter
		SimpleAdapter sAdapter = new SimpleAdapter(this, data, R.layout.category_item, from, to);

		lvMainButtons.setAdapter(sAdapter);

		lvMainButtons.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Log.d(Constants.LOG_APPNAME, "itemClick: position = " + position + ", id = " + id);

				switch (position) {
				case 0:
					// Opens Category activity
					Intent catalogIntent = new Intent(getApplicationContext(), CategoryActivity.class);
					startActivity(catalogIntent);
					break;
				case 1:
					// Opens Map activity
					Intent mapIntent = new Intent(getApplicationContext(), MapActivity.class);
					startActivity(mapIntent);
					break;
				case 2:
					// Opens Feedback activity
					Intent feedbackIntent = new Intent(getApplicationContext(), FeedbackActivity.class);
					startActivity(feedbackIntent);
					break;
				case 3:
					// Opens About company activity
					Intent aboutCompanyIntent = new Intent(getApplicationContext(), AboutcompanyActivity.class);
					startActivity(aboutCompanyIntent);
					break;
				}
			}

		});
	}
}
