package com.maria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.maria.R;
import com.maria.model.Product;
import com.maria.util.ImageFetcher;

public class ProductAdapter extends ArrayAdapter<Product> {

	private LayoutInflater inflater;
	private ImageFetcher mImageFetcher;

	public ProductAdapter(Context context, ImageFetcher imageFetcher, Product[] list) {
		super(context, R.layout.product_item, list);
		inflater = LayoutInflater.from(getContext());
		mImageFetcher = imageFetcher;
	}

	static class ViewHolder {
		TextView title;
		ImageView img;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.product_item, parent, false);

			holder = new ViewHolder();
			holder.title = (TextView) convertView.findViewById(R.id.name);
			holder.img = (ImageView) convertView.findViewById(R.id.image);
			convertView.setTag(holder);
		}
		
		holder = (ViewHolder) convertView.getTag();

		Product item = getItem(position);

		holder.title.setText(item.name);

		if (item.thumbnail_url != null && item.thumbnail_url.startsWith("http")) {
			mImageFetcher.loadThumbnailImage(item.thumbnail_url, holder.img, R.drawable.blank);
			holder.img.setVisibility(View.VISIBLE);
		} else {
			holder.img.setVisibility(View.INVISIBLE);
		}

		return convertView;
	}
}
