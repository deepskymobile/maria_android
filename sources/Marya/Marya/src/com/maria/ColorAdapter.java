package com.maria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.maria.R;
import com.maria.util.ImageFetcher;

public class ColorAdapter extends ArrayAdapter<String> {

	private LayoutInflater inflater;
	private ImageFetcher mImageFetcher;

	public ColorAdapter(Context context, ImageFetcher imageFetcher, String[] colors) {
		super(context, R.layout.gallery_item_photo, colors);
		inflater = LayoutInflater.from(getContext());
		mImageFetcher = imageFetcher;
	}

	static class ViewHolder {
		ImageView img;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.gallery_item_photo, parent, false);

			holder = new ViewHolder();
			holder.img = (ImageView) convertView.findViewById(R.id.image);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

		String item = getItem(position);
		mImageFetcher.loadThumbnailImage(item, holder.img, R.drawable.blank);

		return convertView;
	}
}

