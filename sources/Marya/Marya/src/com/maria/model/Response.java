package com.maria.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.maria.util.LogUtils;

/**
 * Server returns response with xml and error codes
 * 
 * @author Fedor Kazakov
 * 
 */
public class Response<T> implements Parcelable {
	@SuppressWarnings("unused")
	private static final String TAG = LogUtils.makeLogTag("Response");

	private String rawData;
	private String reqType;
	private T data;
	private com.maria.model.Error error;


	/**
	 * Creates response object from received string
	 * 
	 * @param data
	 */
	public Response(String data) {
		rawData = data;
	}

	public Response() {
	}

	public String getRawData() {
		return rawData;
	}

	public void setData(T data) {
		this.data = data;
	}

	public T getData() {
		return data;
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator<Response> CREATOR = new Parcelable.Creator<Response>() {

		@SuppressWarnings("unchecked")
		@Override
		public Response createFromParcel(Parcel source) {
			Response response = new Response();
			response.setReqType(source.readString());
			response.rawData = source.readString();
			response.readData(source);
			return response;
		}

		@Override
		public Response[] newArray(int size) {
			return new Response[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(getReqType());
		dest.writeString(rawData);
		if (data != null) {
			writeData(dest);
		}
	}

	private void writeData(Parcel dest) {
	}

	private void readData(Parcel source) {
	}

	@Override
	public String toString() {
		return "Response reqType " + reqType;
	}

	public String getReqType() {
		return reqType;
	}

	public void setReqType(String reqType) {
		this.reqType = reqType;
	}

	public void setError(com.maria.model.Error error) {
		this.error = error;
		
	}

	public com.maria.model.Error getError() {
		return error;
	}
	
	public static boolean isErrorResponse(@SuppressWarnings("rawtypes") Response response) {
		return response == null || response.getRawData() == null || response.getError() != null;
	}
}
