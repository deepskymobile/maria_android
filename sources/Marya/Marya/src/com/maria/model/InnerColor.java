package com.maria.model;

import android.os.Parcel;
import android.os.Parcelable;

public class InnerColor implements Parcelable {

	public ProductColor product_color;

	public static final Parcelable.Creator<InnerColor> CREATOR = new Parcelable.Creator<InnerColor>() {

		@Override
		public InnerColor createFromParcel(Parcel source) {
			InnerColor result = new InnerColor();
			result.readFromParcel(source);
			return result;
		}

		@Override
		public InnerColor[] newArray(int size) {
			return new InnerColor[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	protected void readFromParcel(Parcel source) {
		ClassLoader loader = getClass().getClassLoader();
		product_color = source.readParcelable(loader);
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeParcelable(product_color, 0);
	}
}
