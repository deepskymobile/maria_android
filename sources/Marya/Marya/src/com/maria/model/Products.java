package com.maria.model;

import java.util.List;

public class Products {

	public Product[] products;
	public int products_count;

	public Products() {

	}

	public Products(List<Product> list) {
		products = list.toArray(new Product[list.size()]);
		products_count = products.length;
	}
}
