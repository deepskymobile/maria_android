package com.maria;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.maria.R;
import com.maria.util.LogUtils;
import com.maria.util.UIUtils;

/**
 * Helper Feedback activity with 2 buttons
 * 
 * @author Fedor Kazakov
 * 
 */
public class FeedbackActivity extends Activity {

	private ListView lvFeedback;

	private boolean userLoggedIn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedback);

		lvFeedback = (ListView) findViewById(android.R.id.list);

		// Creates list items
		CreateListItems();
	}

	/**
	 * Creates list items
	 * 
	 */
	public void CreateListItems() {
		// List items names
		String[] texts = { getResources().getString(R.string.list_call),
				getResources().getString(R.string.list_sendmessage) };
		int[] imgs = { R.drawable.ic_phone, R.drawable.ic_write };

		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(texts.length);
		Map<String, Object> m;
		for (int i = 0; i < texts.length; i++) {
			m = new HashMap<String, Object>();
			m.put(Constants.ATTRIBUTE_NAME_TEXT, texts[i]);
			m.put(Constants.ATTRIBUTE_NAME_IMAGE, imgs[i]);
			data.add(m);
		}

		String[] from = { Constants.ATTRIBUTE_NAME_TEXT, Constants.ATTRIBUTE_NAME_IMAGE };
		int[] to = { R.id.tvText, R.id.ivImg };

		// Creates adapter
		SimpleAdapter sAdapter = new SimpleAdapter(this, data, R.layout.category_item, from, to);

		lvFeedback.setAdapter(sAdapter);
		lvFeedback.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
				case 0:
					doCall();
					break;
				case 1:
					doWrite();
					break;
				}
			}

		});
	}

	private void doCall() {
		UIUtils.questionProceedCancel(this, getString(R.string.call_title), getString(R.string.call_body),
				new OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						// Calls to telephone number from constants
						String url = "tel:" + Constants.CALL_NUMBER;
						try {
							startActivity(new Intent(Intent.ACTION_CALL, Uri.parse(url)));
						} catch (ActivityNotFoundException e) {
							LogUtils.LOGE(Constants.LOG_APPNAME, "Call failed", e);
						}
					}
				}, null);
	}

	private void doWrite() {
		// TODO: Check for user logged in status before opening activity
		userLoggedIn = false;

		// Opens LoginActivity if not logged in or
		// FeedbackformActivity if logged in
		if (userLoggedIn) {
			Intent feedbackformIntent = new Intent(getApplicationContext(), FeedbackformActivity.class);
			startActivity(feedbackformIntent);
		} else {
			Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
			startActivity(loginIntent);
		}
	}
}
