package com.maria;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.maria.R;
import com.google.gson.Gson;
import com.maria.model.Auth;
import com.maria.model.Error;
import com.maria.model.RegisterReqData;
import com.maria.model.Response;
import com.maria.net.RequestsManager;
import com.maria.net.core.events.OnLoaderFinishedEvent;
import com.maria.util.UIUtils;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * Register activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class RegisterActivity extends FragmentActivity {

	private ViewSwitcher switcher;

	private EditText mEtSecondName;
	private EditText mEtFirstName;
	private EditText mEtThirdName;
	private EditText mEtEmail;
	private EditText mEtPassword;
	private EditText mEtPassword2;
	private EditText mEtPhone;
	private EditText mEtAddress;
	private Button mBtnSignIn;

	private RetriableProgressBar progressPane;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		progressPane.setVisibility(View.INVISIBLE);
		switcher = (ViewSwitcher) findViewById(R.id.switcher);

		mEtSecondName = (EditText) findViewById(R.id.et_reg_secondname);
		mEtFirstName = (EditText) findViewById(R.id.et_reg_firstname);
		mEtThirdName = (EditText) findViewById(R.id.et_reg_thirdname);
		mEtEmail = (EditText) findViewById(R.id.et_reg_email);
		mEtPassword = (EditText) findViewById(R.id.et_reg_password);
		mEtPassword2 = (EditText) findViewById(R.id.et_reg_password2);
		mEtPhone = (EditText) findViewById(R.id.et_reg_phone);
		mEtAddress = (EditText) findViewById(R.id.et_reg_address);
		mBtnSignIn = (Button) findViewById(R.id.btn_regSignIn);

		mBtnSignIn.setOnClickListener(oclListener);
	}

	@Override
	public void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
		progressPane.startEventListening();
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_regSignIn:
				Animation shake = AnimationUtils.loadAnimation(RegisterActivity.this, R.anim.shake);
				RegisterReqData dt = new RegisterReqData();
				if ((dt.second_name = UIUtils.textOrError(mEtSecondName, R.string.err_empty_first_name)) == null) {
					mEtSecondName.startAnimation(shake);
					return;
				}
				if ((dt.first_name = UIUtils.textOrError(mEtFirstName, R.string.err_empty_first_name)) == null) {
					mEtFirstName.startAnimation(shake);
					return;
				}

				if ((dt.last_name = UIUtils.textOrNone(mEtThirdName)) == null)
					return;
				if ((dt.login = UIUtils.textOrError(mEtEmail, R.string.err_empty_first_name)) == null) {
					mEtEmail.startAnimation(shake);
					return;
				}
				if ((dt.password = UIUtils.textOrError(mEtPassword, R.string.err_empty_first_name)) == null) {
					mEtPassword.startAnimation(shake);
					return;
				}
				if ((dt.phone = UIUtils.textOrNone(mEtPhone)) == null)
					return;
				if ((dt.address = UIUtils.textOrNone(mEtAddress)) == null)
					return;

				if (!dt.password.equals(UIUtils.textOrNone(mEtPassword2))) {
					Toast.makeText(RegisterActivity.this, R.string.err_unequal_password, Toast.LENGTH_LONG).show();
					mEtPassword.startAnimation(shake);
					mEtPassword2.startAnimation(shake);
					return;
				}
				findViewById(R.id.content).setVisibility(View.GONE);
				Bundle args = new Bundle();
				args.putString(RequestsManager.EXTRA_BODY, new Gson().toJson(dt));
				progressPane.setVisibility(View.VISIBLE);
				UIUtils.startNewLoader(Consts.REQ_REGISTER, args, progressPane, getSupportLoaderManager());
				break;
			}
		}
	};

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_REGISTER.hashCode()) {
			findViewById(R.id.content).setVisibility(View.VISIBLE);
			progressPane.setVisibility(View.INVISIBLE);
			Response<Auth> data = (Response<Auth>) event.getData();
			Error errData = data.getError();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				Intent i = new Intent(this, FeedbackformActivity.class);
				startActivity(i);
				finish();
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		}
	}
}
