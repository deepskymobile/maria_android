package com.maria;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.maria.R;
import com.maria.model.Shop;
import com.maria.util.ImageFetcher;

/**
 * Activity for displaying shop info
 * 
 * @author Fedor Kazakov
 * 
 */
public class ShopDescriptionActivity extends FragmentActivity {

	private Shop shop;
	private ImageFetcher mImageFetcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shop_description);
		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);
		shop = getIntent().getParcelableExtra("shop");
		if (shop != null) {
			setNotNull(R.id.name, R.string.shop_name, shop.name);
			setNotNull(R.id.address, R.string.shop_address, shop.getSnippet());
			setNotNull(R.id.phone, R.string.shop_phone, shop.phone);
			setNotNull(R.id.email, R.string.shop_email, shop.email);
			setNotNull(R.id.description, R.string.shop_working_time, shop.worktime);

			mImageFetcher.loadThumbnailImage(shop.image, (ImageView) findViewById(R.id.image), R.drawable.blank);
		}
	}

	private void setNotNull(int textView, int stringId, String value) {
		if (value == null || value.trim().length() == 0) {
			findViewById(textView).setVisibility(View.GONE);
		} else {
			((TextView) findViewById(textView)).setText(getString(stringId, value));
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		mImageFetcher.flushCache();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}
}
