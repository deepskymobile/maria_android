package com.maria.net;

import org.apache.http.client.methods.HttpRequestBase;

import android.content.Context;

import com.maria.model.Response;

@SuppressWarnings("rawtypes")
public class RequestMock extends RequestLoader<Object> {

	private RequestLoader loader;

	public RequestMock(Context context, RequestLoader loader) {
		super(context, loader.getUrl());
		this.loader = loader;
		body = loader.body;
	}

	@Override
	public HttpRequestBase getRequestBase(String url) {
		return loader.getRequestBase("http://ya.ru");
	}

	@SuppressWarnings("unchecked")
	@Override
	public Response<Object> parseResult(String resultStr) throws Exception {
		resultStr = getMockString();
		return loader.parseResult(resultStr);
	}

	private String getMockString() {
		String resultStr;
		resultStr = "";
		return resultStr;
	}

}
