package com.maria.net;

import static com.maria.util.LogUtils.LOGD;

import java.util.Map.Entry;
import java.util.Set;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;

import com.maria.Consts;
import com.maria.net.core.AbstractHttpLoader;
import com.maria.net.core.MinTimeLoader;
import com.maria.net.core.events.LoaderManagerRetainedEvent;
import com.maria.net.core.events.OnLoaderCreatedEvent;
import com.maria.net.core.events.OnLoaderFinishedEvent;
import com.maria.net.core.events.RetryLoaderEvent;

import de.greenrobot.event.EventBus;

@SuppressWarnings("rawtypes")
public class RequestsManager implements LoaderCallbacks {

	private static final String TAG = "RequestsManager";

	public static final String EXTRA_PASSWORD = "login";
	public static final String EXTRA_LOGIN = "password";
	public static String EXTRA_DELAY = "DELAY";
	public static String EXTRA_MOCK = "MOCK";
	public static String EXTRA_CACHE_FIRST = "CACHE_FIRST";
	public static String EXTRA_BODY = "BODY";

	private Context mContext;

	public RequestsManager(Context context) {
		this.mContext = context;
	}

	public void register() {
		LOGD(TAG, "register");
		EventBus.getDefault().register(this);
	}

	public void unregister() {
		LOGD(TAG, "unregister");
		EventBus.getDefault().unregister(this);
	}

	@SuppressWarnings("unchecked")
	public void onEvent(RequestEvent event) {
		LOGD(TAG, "onEvent( RequestEvent " + event.reqType + ")");
		LoaderManager loaderManager = event.loaderManager;
		Loader<Object> oldLoader = loaderManager.getLoader(event.reqType.hashCode());
		if (oldLoader == null) {
			Loader loader = loaderManager.initLoader(event.reqType.hashCode(), event.args, this);
			loader.forceLoad();
		} else {
			LOGD(TAG, "onEvent existing loader");
			if (((AbstractHttpLoader) oldLoader).isFinished()) {
				Loader loader = loaderManager.restartLoader(event.reqType.hashCode(), event.args, this);
				loader.forceLoad();
			}
		}
	}

	public void onEvent(LoaderManagerRetainedEvent event) {
		LOGD(TAG, "onEvent(LoaderManagerRetainedEvent");
		reInitLoaders(event.getLoaderManager());
	}

	@SuppressWarnings("unchecked")
	public void onEvent(RetryLoaderEvent event) {
		LOGD(TAG, "onEvent(RetryLoaderEvent " + event.getReqType() + ")");
		String reqType = event.getReqType();
		Bundle args = event.getArgs();
		LoaderManager loaderManager = event.getLoaderManager();
		Loader<Object> oldLoader = loaderManager.getLoader(reqType.hashCode());
		if (oldLoader == null) {
			Loader loader = loaderManager.initLoader(reqType.hashCode(), args, this);
			loader.forceLoad();
		} else {
			LOGD(TAG, "onEvent existing loader");
			if (((AbstractHttpLoader) oldLoader).isFinished()) {
				Loader loader = loaderManager.initLoader(reqType.hashCode(), args, this);
				loader.forceLoad();
			}
		}
	}

	@Override
	public Loader onCreateLoader(int id, Bundle args) {
		LOGD(TAG, "onCreateLoader(" + id + ")");
		EventBus.getDefault().post(new OnLoaderCreatedEvent(id, args));
		return _onCreateLoader(id, args);
	}

	@SuppressWarnings("unchecked")
	private Loader _onCreateLoader(int id, Bundle args) {
		RequestLoader result;
		long delay = 0;
		boolean mock = false;
		boolean cacheFirst = false;
		String body = null;
		Set<Entry<String, Class<?>>> entrySet = Consts.responseClasses.entrySet();
		String reqType = null;
		for (Entry<String, Class<?>> entry : entrySet) {
			if (entry.getKey().hashCode() == id) {
				reqType = entry.getKey();
			}
		}

		if (args.containsKey(EXTRA_DELAY)) {
			delay = args.getLong(EXTRA_DELAY);
			args.remove(EXTRA_DELAY);
		}

		if (args.containsKey(EXTRA_MOCK)) {
			mock = args.getBoolean(EXTRA_MOCK);
			args.remove(EXTRA_MOCK);
		}

		if (args.containsKey(EXTRA_BODY)) {
			body = args.getString(EXTRA_BODY);
			args.remove(EXTRA_BODY);
		}

		if (args.containsKey(EXTRA_CACHE_FIRST)) {
			cacheFirst = args.getBoolean(EXTRA_CACHE_FIRST);
			args.remove(EXTRA_CACHE_FIRST);
		}

		result = new RequestLoader(mContext, Consts.HOST + reqType, args, body, getRootClass(reqType));

		if (delay > 0) {
			result = new MinTimeLoader(mContext, result, delay);
		}

		result.setCacheable(isCacheableRequest(reqType));
		result.setCacheFirst(cacheFirst);
		
		if (mock) {
			return new RequestMock(mContext, result);
		} else {
			return result;
		}
	}

	private boolean isCacheableRequest(String reqType) {
		return Consts.canCacheReqType.contains(reqType);
	}

	public Class getRootClass(String reqType) {
		Class result = Consts.responseClasses.get(reqType);
		if (result == null)
			throw new IllegalArgumentException("reqType not supported");
		return result;
	}

	@SuppressWarnings("unchecked")
	public void reInitLoaders(LoaderManager loaderManager) {
		Set<Entry<String, Class<?>>> set = Consts.responseClasses.entrySet();
		for (Entry<String, Class<?>> entry : set) {
			String key = entry.getKey();
			if (loaderManager.getLoader(key.hashCode()) != null) {
				loaderManager.initLoader(key.hashCode(), null, this);
			}
		}
	}

	@Override
	public void onLoadFinished(Loader loader, Object data) {
		LOGD(TAG, "onLoadFinished(" + loader.getId() + " " + (data != null) + ")");
		((AbstractHttpLoader) loader).setFinished(true);
		EventBus.getDefault().post(new OnLoaderFinishedEvent(loader.getId(), data));
	}

	@Override
	public void onLoaderReset(Loader loader) {
		LOGD(TAG, "onLoadReset()");
	}

	public static int getLoadingRequestsCount(LoaderManager loaderManager) {
		int result = 0;
		Set<Entry<String, Class<?>>> set = Consts.responseClasses.entrySet();
		for (Entry<String, Class<?>> entry : set) {
			String reqType = entry.getKey();
			RequestLoader loader = (RequestLoader) loaderManager.getLoader(reqType.hashCode());
			if (loader != null && !loader.isFinished()) {
				result++;
			}
		}
		return result;
	}
}
