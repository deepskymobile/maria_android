package com.maria.net;

import static com.maria.util.LogUtils.LOGD;
import static com.maria.util.LogUtils.LOGE;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import android.content.Context;
import android.os.Bundle;

import com.google.gson.Gson;
import com.maria.model.Response;
import com.maria.net.core.AbstractHttpLoader;
import com.maria.util.Utils;

/**
 * Initial request, requires login and password
 * 
 * @author fkazakov
 * 
 */
public class RequestLoader<T> extends AbstractHttpLoader<Response<T>> {

	private Class<? extends T> type;
	private String reqType;
	private boolean isGet;

	public RequestLoader(Context context, String url) {
		super(context, url);
	}

	public RequestLoader(Context context, String url, Bundle args, String bodyStr, Class<? extends T> type) {
		super(context, url);
		this.type = type;
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(args.size());
		Set<String> keys = args.keySet();
		if (keys.size() > 0) {
			StringBuilder sb = new StringBuilder();
			sb.append(getUrl());
			sb.append('?');
			for (String key : keys) {
				nameValuePairs.add(new BasicNameValuePair(key, args.get(key).toString()));
				sb.append(key).append('=').append(args.get(key).toString()).append("&");
			}
			sb.deleteCharAt(sb.length() - 1);
			setUrl(sb.toString());
		}
		if (bodyStr != null) {
			isGet = false;
			try {
				body = new StringEntity(bodyStr);
			} catch (UnsupportedEncodingException e) {
				LOGE(TAG, "UnsupportedEncodingException ", e);
			}
		} else {
			isGet = true;
		}
	}

	@Override
	public String getDbKey() {
		try {
			return Utils.md5(getUrl());
		} catch (IllegalStateException e) {
			e.printStackTrace();
			return "";
		}
	}

	@Override
	public Response<T> parseResult(String resultStr) throws Exception {
		Response<T> response = new Response<T>(resultStr);
		response.setReqType(reqType);

		// For some requests there's nothing to parse in response
		if (type.equals(Void.class))
			return response;

		try {
			if (responseCode == 422) {
				response.setError(parseError(response));
			}
			response.setData(parseData(response));
		} catch (Exception e) {
			LOGE(TAG, "Parse error ", e);
		}
		return response;
	}

	private com.maria.model.Error parseError(Response<T> response) {
		com.maria.model.Error data = null;
		try {
			Gson gson = new Gson();
			data = gson.fromJson(response.getRawData(), com.maria.model.Error.class);
		} catch (Exception e) {
			LOGE(TAG, "Parse error ", e);
		}
		return data;
	}

	private T parseData(Response<T> response) throws Exception {
		T data = null;
		try {
			Gson gson = new Gson();
			data = gson.fromJson(response.getRawData(), type);
			LOGD(TAG, "" + (data == null));
		} catch (Exception e) {
			LOGE(TAG, "Parse error ", e);
		}
		return data;
	}

	public String getReqType() {
		return reqType;
	}

	@Override
	public HttpRequestBase getRequestBase(String url) {
		return isGet ? new HttpGet(url) : new HttpPost(url);
	}

}
