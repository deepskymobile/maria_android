package com.maria.net.core;

import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;

import android.content.Context;

public abstract class HttpPostLoader<D> extends AbstractHttpLoader<D> {

	public HttpPostLoader(Context context, String url) {
		super(context, url);
	}

	@Override
	public HttpRequestBase getRequestBase(String url) {
		return new HttpPost(url);
	}
}
