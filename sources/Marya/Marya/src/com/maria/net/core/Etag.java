package com.maria.net.core;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Http Etag class
 * 
 * @author Fedor Kazakov
 */
@DatabaseTable
public class Etag {

    @DatabaseField(id = true)
    String url;
    @DatabaseField
    String value;

    public Etag() {
    }
    
	public Etag(String url) {
        this.url = url;
    }
    
    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }

}
