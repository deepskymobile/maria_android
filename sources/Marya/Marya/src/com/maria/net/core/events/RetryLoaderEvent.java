package com.maria.net.core.events;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;

public class RetryLoaderEvent {

	private String reqType;
	private Bundle args;
	private LoaderManager loaderManager;

	public RetryLoaderEvent(String reqType, Bundle args, LoaderManager loaderManager) {
		this.reqType = reqType;
		this.args = args;
		this.loaderManager = loaderManager;
	}

	public String getReqType() {
		return reqType;
	}

	public Bundle getArgs() {
		return args;
	}

	public LoaderManager getLoaderManager() {
		return loaderManager;
	}

}
