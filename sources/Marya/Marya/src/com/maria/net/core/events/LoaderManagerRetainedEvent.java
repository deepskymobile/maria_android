package com.maria.net.core.events;

import android.support.v4.app.LoaderManager;

public class LoaderManagerRetainedEvent {

	private LoaderManager loaderManager;

	public LoaderManagerRetainedEvent(LoaderManager supportLoaderManager) {
		this.loaderManager = supportLoaderManager;
	}
	
	public LoaderManager getLoaderManager() {
		return loaderManager;
	}


}
