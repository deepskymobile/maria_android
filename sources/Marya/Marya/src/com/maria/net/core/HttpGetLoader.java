package com.maria.net.core;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;

import android.content.Context;

public abstract class HttpGetLoader<D> extends AbstractHttpLoader<D> {

	public HttpGetLoader(Context context, String url) {
		super(context, url);
	}

	@Override
	public HttpRequestBase getRequestBase(String url) {
		return new HttpGet(url);
	}

}
