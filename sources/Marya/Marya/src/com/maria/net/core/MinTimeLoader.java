package com.maria.net.core;

import org.apache.http.client.methods.HttpRequestBase;

import com.maria.model.Response;
import com.maria.net.RequestLoader;

import android.content.Context;

public class MinTimeLoader<T> extends RequestLoader<T> {

	private long timestamp;
	private long minTime;
	private RequestLoader<T> loader;

	public MinTimeLoader(Context context, RequestLoader<T> loader, long minTime) {
		super(context, loader.getUrl());
		timestamp = System.currentTimeMillis();
		this.minTime = minTime;
		this.loader = loader;
		body = loader.body;
	}

	@Override
	public Response<T> parseResult(String resultStr) throws Exception {
		Response<T> result = loader.parseResult(resultStr);
		if (result != null) {
			while (System.currentTimeMillis() - timestamp < minTime) {
				Thread.sleep(100);
			}
		}
		return result;
	}

	@Override
	public HttpRequestBase getRequestBase(String url) {
		return loader.getRequestBase(url);
	}

}
