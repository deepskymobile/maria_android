package com.maria.net.core;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Http cache class
 * 
 * @author Fedor Kazakov
 */
@DatabaseTable
public class HttpResponseCache {

	@DatabaseField(id = true)
	String url;
	@DatabaseField
	String data;

	public HttpResponseCache() {
	}

	public HttpResponseCache(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(final String url) {
		this.url = url;
	}

	public String getData() {
		return data;
	}

	public void setData(final String value) {
		this.data = value;
	}

}
