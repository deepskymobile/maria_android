package com.maria.net.core;

/**
 * Interface for refresh event listeners
 * 
 * @author Fedor Kazakov
 */
public interface OnRefreshListener {

	public void onRefresh();
}
