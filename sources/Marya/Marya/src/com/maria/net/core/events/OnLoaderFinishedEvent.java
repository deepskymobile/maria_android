package com.maria.net.core.events;

public class OnLoaderFinishedEvent {

	private int id;
	private Object data;

	public OnLoaderFinishedEvent(int id, Object data) {
		this.id = id;
		this.data = data;
	}

	public int getId() {
		return id;
	}

	public Object getData() {
		return data;
	}

}
