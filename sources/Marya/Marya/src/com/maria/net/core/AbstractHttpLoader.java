package com.maria.net.core;

import static com.maria.util.LogUtils.LOGD;
import static com.maria.util.LogUtils.LOGE;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.Serializable;
import java.net.URLDecoder;
import java.util.List;
import java.util.zip.GZIPInputStream;

import org.apache.http.Header;
import org.apache.http.HeaderElement;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.content.Context;
import android.content.SharedPreferences.Editor;
import android.support.v4.content.AsyncTaskLoader;
import android.widget.Toast;

import com.maria.R;
import com.j256.ormlite.dao.RuntimeExceptionDao;
import com.maria.App;
import com.maria.Consts;
import com.maria.util.LogUtils;
import com.maria.util.Utils;

public abstract class AbstractHttpLoader<D> extends AsyncTaskLoader<D> {

	public static final String ENCODING = "UTF-8";

	protected static final String TAG = LogUtils.makeLogTag(AbstractHttpLoader.class);

	private static final String HTTP_HEADER_IF_NONE_MATCH = "If-None-Match";
	private static final String HTTP_HEADER_ETAG = "ETag";

	protected Context context;
	protected boolean cleanEtag;
	protected boolean refresh;
	protected int responseCode;
	public HttpEntity body;
	private boolean isFinished = false;

	private String url;
	private Runnable onPostExecuteListener;

	private static RuntimeExceptionDao<Etag, Integer> mDb;

	private static RuntimeExceptionDao<HttpResponseCache, Integer> mCacheDb;

	private boolean isCacheable;

	private boolean cacheFirst;

	private static HttpContext localContext;

	public AbstractHttpLoader(Context context, final String url) {
		super(context);
		this.context = context;
		this.url = url;
		mDb = App.getHelper().getRuntimeExceptionDao(Etag.class);
		mCacheDb = App.getHelper().getRuntimeExceptionDao(HttpResponseCache.class);
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	@Override
	public D loadInBackground() {
		String reqDbKey = getDbKey();
		boolean fromCache = false;
		String resultStr = null;

		if (cacheFirst) {
			LOGD(TAG, "Get from cache, no download");
			resultStr = getFromCache(reqDbKey);
			fromCache = true;
		}

		if (resultStr == null) {
			resultStr = download(url);
			fromCache = false;
		}

		if (isCacheable()) { // request can work with offline cache
			if (resultStr == null) { // request failed, try to lookup in cache
				resultStr = getFromCache(reqDbKey);
				fromCache = true;
			}
		}

		D result = null;
		if (isAbandoned())
			return null;
		try {
			result = parseResult(resultStr);
			// caching here means we are not failed during parsing
			if (isCacheable() && !fromCache)
				putToCache(reqDbKey, resultStr);
		} catch (Exception e) {
			LOGE(TAG, "Response parsing error", e);
			// let's try same with cached result
			if (isCacheable() && !fromCache) {
				try {
					result = parseResult(getFromCache(reqDbKey));
				} catch (Exception e2) {
					responseCode = -1;
				}
			} else {
				responseCode = -1;
			}
		}
		return result;
	}

	public String getDbKey() {
		return this.getClass().getCanonicalName();
	}

	@Override
	public void onContentChanged() {
		LOGD(TAG, "onContentChanged()");
		super.onContentChanged();
	}

	@Override
	protected void onAbandon() {
		LOGD(TAG, "onAbandon()");
		super.onAbandon();
	}

	@Override
	protected void onForceLoad() {
		LOGD(TAG, "onForceLoad()");
		super.onForceLoad();
	}

	@Override
	protected void onReset() {
		LOGD(TAG, "onReset()");
		super.onReset();
	}

	@Override
	protected D onLoadInBackground() {
		LOGD(TAG, "onLoadInBackground()");
		return super.onLoadInBackground();

	}

	@Override
	public void onCanceled(D data) {
		LOGD(TAG, "onCanceled(D data)");
		super.onCanceled(data);
	}

	@Override
	protected void onStartLoading() {
		LOGD(TAG, "onStartLoading()");
		super.onStartLoading();
	}

	@Override
	protected void onStopLoading() {
		LOGD(TAG, "onStopLoading()");
		super.onStopLoading();
	}

	private void putToCache(String key, String value) {
		LOGD(TAG, "putToCache(" + key + " )");
		HttpResponseCache cache = new HttpResponseCache(key);
		cache.data = value;
		mCacheDb.createOrUpdate(cache);
	}

	private String getFromCache(String key) {
		LOGD(TAG, "getFromCache(" + key + " )");
		HttpResponseCache cache = mCacheDb.queryForSameId(new HttpResponseCache(key));
		if (cache != null) {
			return cache.data;
		}
		return null;
	}

	public abstract D parseResult(String resultStr) throws Exception;

	protected String download(final String url) {
		String reply = null;
		try {
			reply = makeRequest(url);
		} catch (final IOException e) {
			responseCode = -1;
			LOGE(TAG, "Networking error", e);
		}
		refresh = reply != null;
		return reply;
	}

	private String makeRequest(final String url) throws IOException {
		Etag etag = mDb.queryForSameId(new Etag(url));
		if (etag != null) {
			if (cleanEtag) {
				etag = new Etag(url);
				mDb.delete(etag);
				cleanEtag = false;
			}
		} else {
			etag = new Etag();
			etag.setUrl(url);
			etag.setValue("");
		}

		if (isAbandoned())
			return null;

		final HttpClient httpClient = new MyHttpClient();
		final HttpRequestBase request = getRequestBase(url);
		request.setHeader("Content-Type", "application/json");
		request.addHeader("Accept-Encoding", "gzip");

		LOGD(TAG, "[" + request.getMethod() + "] " + url);

		if (etag != null && etag.getValue() != null) {
			request.addHeader(HTTP_HEADER_IF_NONE_MATCH, "\"" + etag.getValue() + "\"");
		}

		Header[] headers = request.getAllHeaders();
		if (headers != null && headers.length > 0) {
			for (Header header : headers) {
				LOGD(TAG, "[Header] " + header.getName() + ": " + header.getValue());
			}
		}

		if (body != null) {
			LOGD(TAG, "[BODY] " + URLDecoder.decode(Utils.readInputStreamAsString(body.getContent()), ENCODING));
			if (request.getMethod().equals("POST")) {
				((HttpPost) request).setEntity(body);
			}
		}

		HttpContext localContext = getHttpContext();

		HttpResponse response = httpClient.execute(request, localContext);

		if (isAbandoned())
			return null;

		responseCode = response.getStatusLine().getStatusCode();

		LOGD(TAG, "[RESPONSE] httpCode: " + responseCode);

		if ((responseCode != 304 && responseCode < 400) || (responseCode == 422)) {
			return readHttpResponse(response, etag);
		} else {
			return null;
		}
	}

	private static HttpContext getHttpContext() {
		if (localContext == null) {
			localContext = new BasicHttpContext();
			// Create a local instance of cookie store
			CookieStore cookieStore = new BasicCookieStore();
			// Bind custom cookie store to the local context
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		}
		return localContext;
	}

	protected void errorMessage() {
		Toast.makeText(context, R.string.network_error, Toast.LENGTH_LONG).show();
	}

	protected void onErrorResponse() {
		errorMessage();
	}

	protected String readHttpResponse(final HttpResponse response, final Etag etag) throws IOException {
		String newEtagValue = null;
		final Header[] tags = response.getHeaders(HTTP_HEADER_ETAG);
		if (tags != null && tags.length > 0) {
			newEtagValue = tags[0].getValue().replace("\"", "");
		}

		String responseBody = _getResponseBody(response);

		if (isAbandoned())
			return null;

		if (newEtagValue == null || !newEtagValue.equals(etag.getValue())) {
			if (newEtagValue != null) {
				etag.setValue(newEtagValue);
				mDb.createOrUpdate(etag);
			}
			LOGD(TAG, "[RESPONSE]: size=" + (responseBody.length() / 1024) + "KB " + responseBody);
			return responseBody;
		} else {
			return null;
		}
	}

	private String _getResponseBody(final HttpResponse response) throws IOException {
		HttpEntity entity = response.getEntity();
		if (entity == null) {
			throw new IllegalArgumentException("HTTP entity may not be null");
		}
		InputStream instream = entity.getContent();
		Header contentEncoding = response.getFirstHeader("Content-Encoding");
		if (contentEncoding != null && "gzip".equalsIgnoreCase(contentEncoding.getValue())) {
			instream = new GZIPInputStream(instream);
		}
		if (instream == null) {
			return "";
		}
		if (entity.getContentLength() > Integer.MAX_VALUE) {
			throw new IllegalArgumentException("HTTP entity too large to be buffered in memory");
		}
		String charset = getContentCharSet(entity);
		if (charset == null) {
			charset = ENCODING;
		}
		Reader reader = new InputStreamReader(instream, charset);
		StringBuilder buffer = new StringBuilder();
		try {
			char[] tmp = new char[1024];
			int l;
			while ((l = reader.read(tmp)) != -1) {
				buffer.append(tmp, 0, l);

				if (isAbandoned())
					return null;

			}
		} finally {
			reader.close();
		}

		if (isAbandoned())
			return null;

		return buffer.toString();
	}

	public static boolean isGZip(final byte[] data) {
		// 0x00008b1f
		return data != null && data.length > 2 && (data[0] & 0xff) == 0x1f && (data[1] & 0xff) == 0x8b;
	}

	private String getContentCharSet(final HttpEntity entity) {
		if (entity == null) {
			throw new IllegalArgumentException("HTTP entity may not be null");
		}
		String charset = null;
		if (entity.getContentType() != null) {
			HeaderElement values[] = entity.getContentType().getElements();
			if (values.length > 0) {
				NameValuePair param = values[0].getParameterByName("charset");
				if (param != null) {
					charset = param.getValue();
				}
			}
		}
		return charset;
	}

	public abstract HttpRequestBase getRequestBase(String url);

	public Runnable getOnPostExecuteListener() {
		return onPostExecuteListener;
	}

	public void setOnPostExecuteListener(Runnable onPostExecuteListener) {
		this.onPostExecuteListener = onPostExecuteListener;
	}

	public boolean isCleanEtag() {
		return cleanEtag;
	}

	public void setCleanEtag(boolean cleanEtag) {
		this.cleanEtag = cleanEtag;
	}

	public boolean isFinished() {
		return isFinished;
	}

	public void setFinished(boolean isFinished) {
		this.isFinished = isFinished;
	}

	public boolean isCacheable() {
		return isCacheable;
	}

	public void setCacheable(boolean isCacheable) {
		this.isCacheable = isCacheable;
	}

	public void setCacheFirst(boolean cacheFirst) {
		this.cacheFirst = cacheFirst;
	}

	public static boolean hasCookies() {
		if (localContext == null)
			return false;
		CookieStore cookieStore = (CookieStore) localContext.getAttribute(ClientContext.COOKIE_STORE);
		if (cookieStore == null)
			return false;
		List<Cookie> cookies = cookieStore.getCookies();
		for (Cookie cookie : cookies) {
			if (cookie.getDomain().contains("marya"))
				return true;
		}
		return false;
	}

	public static void saveCookies(Context context) {
		if (localContext == null)
			return;
		CookieStore cookieStore = (CookieStore) localContext.getAttribute(ClientContext.COOKIE_STORE);
		if (cookieStore == null)
			return;
		StringBuilder sb = new StringBuilder();
		List<Cookie> cookies = cookieStore.getCookies();

		if (cookies == null || cookies.size() == 0)
			return;

		for (Cookie cookie : cookies) {
			if (cookie.getDomain().contains("marya")) {
				try {
					String cookieString = toString(new SerializableCookie(cookie));
					sb.append(cookieString);
					sb.append(',');
				} catch (IOException e) {
				}
			}
		}
		sb.deleteCharAt(sb.length() - 1);
		context.getApplicationContext().getSharedPreferences(Consts.SETTINGS_NAME, Context.MODE_PRIVATE).edit()
				.putString("cookies", sb.toString()).apply();

	}

	public static void readCookies(Context context) {
		if (localContext == null)
			getHttpContext();
		CookieStore cookieStore = (CookieStore) localContext.getAttribute(ClientContext.COOKIE_STORE);
		if (cookieStore == null)
			return;
		String cookiesStr = context.getApplicationContext()
				.getSharedPreferences(Consts.SETTINGS_NAME, Context.MODE_PRIVATE).getString("cookies", null);
		if (cookiesStr == null)
			return;
		String[] cookies = cookiesStr.split(",");
		for (int i = 0; i < cookies.length; i++) {
			SerializableCookie sc;
			try {
				sc = (SerializableCookie) fromString(cookies[i]);
				cookieStore.addCookie(sc.getCookie());
			} catch (IOException e) {
			} catch (ClassNotFoundException e) {
			}
		}
	}

	public static void removeCookies(Context context) {
		Editor editor = context.getApplicationContext()
				.getSharedPreferences(Consts.SETTINGS_NAME, Context.MODE_PRIVATE).edit();
		editor.remove("cookies").commit();
		CookieStore cookieStore = (CookieStore) localContext.getAttribute(ClientContext.COOKIE_STORE);
		if (cookieStore == null)
			return;
		cookieStore.clear();
	}

	/** Read the object from Base64 string. */
	private static Object fromString(String s) throws IOException, ClassNotFoundException {
		byte[] data = Base64Coder.decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	/** Write the object to a Base64 string. */
	private static String toString(Serializable o) throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ObjectOutputStream oos = new ObjectOutputStream(baos);
		oos.writeObject(o);
		oos.close();
		return new String(Base64Coder.encode(baos.toByteArray()));
	}
}
