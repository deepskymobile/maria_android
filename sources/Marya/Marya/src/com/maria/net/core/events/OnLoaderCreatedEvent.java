package com.maria.net.core.events;

import android.os.Bundle;

public class OnLoaderCreatedEvent {

	private int id;
	private Bundle args;

	public OnLoaderCreatedEvent(int id, Bundle args) {
		this.id = id;
		this.args = args;
	}

	public int getId() {
		return id;
	}

	public Bundle getArgs() {
		return args;
	}

}
