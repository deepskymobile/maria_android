package com.maria.util;

import java.util.Calendar;
import java.util.TimeZone;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.LoaderManager;
import android.text.Html;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.maria.BuildConfig;
import com.maria.R;
import com.maria.net.RequestEvent;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * An assortment of UI helpers.
 */
public class UIUtils {

	public static final TimeZone CURRENT_TIME_ZONE = TimeZone.getTimeZone("Europe/Moscow");

	private static final int SECOND_MILLIS = 1000;
	private static final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
	private static final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
	private static final int DAY_MILLIS = 24 * HOUR_MILLIS;

	/** Flags used with {@link DateUtils#formatDateRange}. */
	private static final int TIME_FLAGS = DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_WEEKDAY
			| DateUtils.FORMAT_ABBREV_WEEKDAY;

	/**
	 * Format and return the given {@link Blocks} values using {@link #CURRENT_TIME_ZONE}.
	 */
	public static String formatBlockTimeString(long blockStart, long blockEnd, Context context) {
		TimeZone.setDefault(CURRENT_TIME_ZONE);

		// NOTE: There is an efficient version of formatDateRange in Eclair and
		// beyond that allows you to recycle a StringBuilder.
		return DateUtils.formatDateRange(context, blockStart, blockEnd, TIME_FLAGS);
	}

	public static boolean isSameDay(long time1, long time2) {
		TimeZone.setDefault(CURRENT_TIME_ZONE);

		Calendar cal1 = Calendar.getInstance();
		Calendar cal2 = Calendar.getInstance();
		cal1.setTimeInMillis(time1);
		cal2.setTimeInMillis(time2);
		return cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR)
				&& cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
	}

	public static String getTimeAgo(long time, Context ctx) {
		if (time < 1000000000000L) {
			// if timestamp given in seconds, convert to millis
			time *= 1000;
		}

		long now = getCurrentTime(ctx);
		if (time > now || time <= 0) {
			return null;
		}

		// TODO: localize
		final long diff = now - time;
		if (diff < MINUTE_MILLIS) {
			return "just now";
		} else if (diff < 2 * MINUTE_MILLIS) {
			return "a minute ago";
		} else if (diff < 50 * MINUTE_MILLIS) {
			return diff / MINUTE_MILLIS + " minutes ago";
		} else if (diff < 90 * MINUTE_MILLIS) {
			return "an hour ago";
		} else if (diff < 24 * HOUR_MILLIS) {
			return diff / HOUR_MILLIS + " hours ago";
		} else if (diff < 48 * HOUR_MILLIS) {
			return "yesterday";
		} else {
			return diff / DAY_MILLIS + " days ago";
		}
	}

	/**
	 * Populate the given {@link TextView} with the requested text, formatting through {@link Html#fromHtml(String)}
	 * when applicable. Also sets {@link TextView#setMovementMethod} so inline links are handled.
	 */
	public static void setTextMaybeHtml(TextView view, String text) {
		if (TextUtils.isEmpty(text)) {
			view.setText("");
			return;
		}
		if (text.contains("<") && text.contains(">")) {
			view.setText(Html.fromHtml(text));
			view.setMovementMethod(LinkMovementMethod.getInstance());
		} else {
			view.setText(text);
		}
	}

	public static void preferPackageForIntent(Context context, Intent intent, String packageName) {
		PackageManager pm = context.getPackageManager();
		for (ResolveInfo resolveInfo : pm.queryIntentActivities(intent, 0)) {
			if (resolveInfo.activityInfo.packageName.equals(packageName)) {
				intent.setPackage(packageName);
				break;
			}
		}
	}

	private static final int BRIGHTNESS_THRESHOLD = 130;

	/**
	 * Calculate whether a color is light or dark, based on a commonly known brightness formula.
	 * 
	 * @see {@literal http://en.wikipedia.org/wiki/HSV_color_space%23Lightness}
	 */
	public static boolean isColorDark(int color) {
		return ((30 * Color.red(color) + 59 * Color.green(color) + 11 * Color.blue(color)) / 100) <= BRIGHTNESS_THRESHOLD;
	}

	// Shows whether a notification was fired for a particular session time
	// block. In the
	// event that notification has not been fired yet, return false and set the
	// bit.
	public static boolean isNotificationFiredForBlock(Context context, String blockId) {
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
		final String key = String.format("notification_fired_%s", blockId);
		boolean fired = sp.getBoolean(key, false);
		sp.edit().putBoolean(key, true).commit();
		return fired;
	}

	private static final long sAppLoadTime = System.currentTimeMillis();

	public static long getCurrentTime(final Context context) {
		if (BuildConfig.DEBUG) {
			return context.getSharedPreferences("mock_data", Context.MODE_PRIVATE).getLong("mock_current_time",
					System.currentTimeMillis())
					+ System.currentTimeMillis() - sAppLoadTime;
		} else {
			return System.currentTimeMillis();
		}
	}

	public static void safeOpenLink(Context context, Intent linkIntent) {
		try {
			context.startActivity(linkIntent);
		} catch (ActivityNotFoundException e) {
			Toast.makeText(context, "Couldn't open link", Toast.LENGTH_SHORT).show();
		}
	}

	// TODO: use <meta-data> element instead
	@SuppressWarnings("rawtypes")
	private static final Class[] sPhoneActivities = new Class[] {};

	// TODO: use <meta-data> element instead
	@SuppressWarnings("rawtypes")
	private static final Class[] sTabletActivities = new Class[] {};

	@SuppressWarnings("rawtypes")
	public static void enableDisableActivities(final Context context) {
		boolean isHoneycombTablet = isHoneycombTablet(context);
		PackageManager pm = context.getPackageManager();

		// Enable/disable phone activities
		for (Class a : sPhoneActivities) {
			pm.setComponentEnabledSetting(new ComponentName(context, a),
					isHoneycombTablet ? PackageManager.COMPONENT_ENABLED_STATE_DISABLED
							: PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
		}

		// Enable/disable tablet activities
		for (Class a : sTabletActivities) {
			pm.setComponentEnabledSetting(new ComponentName(context, a),
					isHoneycombTablet ? PackageManager.COMPONENT_ENABLED_STATE_ENABLED
							: PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static void setActivatedCompat(View view, boolean activated) {
		if (hasHoneycomb()) {
			view.setActivated(activated);
		}
	}

	public static boolean hasFroyo() {
		// Can use static final constants like FROYO, declared in later versions
		// of the OS since they are inlined at compile time. This is guaranteed
		// behavior.
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
	}

	public static boolean hasGingerbread() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD;
	}

	public static boolean hasHoneycomb() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
	}

	public static boolean hasHoneycombMR1() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR1;
	}

	public static boolean hasICS() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH;
	}

	public static boolean hasJellyBean() {
		return Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN;
	}

	public static boolean isTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
	}

	public static boolean isHoneycombTablet(Context context) {
		return hasHoneycomb() && isTablet(context);
	}

	public static void setLockOrientation(Activity a) {
		// if (UIUtils.isHoneycombTablet(a)) {
		// a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
		// } else {
		// a.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
		// }
	}

	public static void questionProceedCancel(final Context context, final String title, final CharSequence message,
			final DialogInterface.OnClickListener onClickYes, final DialogInterface.OnClickListener onClickNo) {
		new AlertDialog.Builder(context).setTitle(title).setMessage(message).setCancelable(true)
				.setPositiveButton(R.string.contin, onClickYes).setNegativeButton(R.string.cancel, onClickNo).show();
	}

	public static void alertOk(final Context context, final String title, final CharSequence message) {
		new AlertDialog.Builder(context).setTitle(title).setMessage(message).setCancelable(false)
				.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
					public void onClick(final DialogInterface dialog, final int id) {
						dialog.cancel();
					}
				}).show();
	}

	public static void startNewLoader(String reqType, Bundle args, RetriableProgressBar bar, LoaderManager loaderManager) {
		if (bar != null)
			bar.attachActions(reqType, args);
		RequestEvent event = new RequestEvent(loaderManager, reqType, args);
		EventBus.getDefault().post(event);
	}

	public static void disableEnableControls(boolean enable, ViewGroup vg) {
		for (int i = 0; i < vg.getChildCount(); i++) {
			View child = vg.getChildAt(i);
			child.setEnabled(enable);
			if (child instanceof ViewGroup) {
				disableEnableControls(enable, (ViewGroup) child);
			}
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isConnected()) {
			return true;
		}
		return false;
	}

	public static String textOrError(EditText edit, int errId) {
		String text = edit.getText().toString().trim();
		if (text.length() == 0) {
			if (errId != 0)
				Toast.makeText(edit.getContext(), edit.getContext().getString(errId), Toast.LENGTH_LONG).show();
			return null;
		}
		return text;
	}
	
	public static String textOrNone(EditText edit) {
		String text = edit.getText().toString().trim();
		return text;
	}
	
	public static boolean isValidImageUrl(String url) {
		return url != null && (url.startsWith("http") || url.startsWith("/"));
	}
}
