package com.megatron.widget;

import static com.maria.util.LogUtils.LOGD;
import static com.maria.util.LogUtils.LOGE;

import com.maria.R;
import com.maria.net.core.events.OnLoaderCreatedEvent;
import com.maria.net.core.events.OnLoaderFinishedEvent;
import com.maria.net.core.events.RetryLoaderEvent;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import de.greenrobot.event.EventBus;

public class RetriableProgressBar extends LinearLayout {

	private static final String TAG = "RetriableProgressBar";

	private static final int TRY_AGAIN_DELAY_MILLIS = 10000;
	private static final String XMLNS = "http://github.com/megatronxxx/schema";

	private Handler mHandler;

	private int loaderId;

	private View progressBar;
	private TextView loadingTextView;
	private Button tryAgainButton;

	// attributes
	private String textLoading = "Loading...";
	private String textLoadingTooLong = "Loading too long. Retry?";
	private String textError = "Error";
	private String textEmpty = "Empty";
	private String textRetry = "Retry";
	private int delayMs = TRY_AGAIN_DELAY_MILLIS;
	private ProgressState state = ProgressState.SUCCESS;
	private String reqType = "";
	private Bundle args;

	interface State {
		public void applyTo(RetriableProgressBar bar);
	}

	public enum ProgressState implements State {
		LOADING {
			@Override
			public void applyTo(RetriableProgressBar bar) {
				bar.setVisibility(View.VISIBLE);
				bar.progressBar.setVisibility(View.VISIBLE);
				bar.loadingTextView.setText(bar.textLoading);
				bar.tryAgainButton.setVisibility(View.GONE);
			}
		},
		LOADING_TOO_LONG {
			@Override
			public void applyTo(RetriableProgressBar bar) {
				bar.setVisibility(View.VISIBLE);
				bar.progressBar.setVisibility(View.VISIBLE);
				bar.loadingTextView.setText(bar.textLoadingTooLong);
				bar.tryAgainButton.setVisibility(View.VISIBLE);
			}
		},
		ERROR {
			@Override
			public void applyTo(RetriableProgressBar bar) {
				bar.setVisibility(View.VISIBLE);
				bar.progressBar.setVisibility(View.GONE);
				bar.loadingTextView.setText(bar.textError);
				bar.tryAgainButton.setVisibility(View.VISIBLE);
			}
		},
		SUCCESS {
			@Override
			public void applyTo(RetriableProgressBar bar) {
				bar.setVisibility(View.GONE);
			}
		},
		EMPTY {
			@Override
			public void applyTo(RetriableProgressBar bar) {
				bar.setVisibility(View.VISIBLE);
				bar.progressBar.setVisibility(View.GONE);
				bar.loadingTextView.setText(bar.textEmpty);
				bar.tryAgainButton.setVisibility(View.GONE);
			}
		}
	}

	static class SavedState extends BaseSavedState {
		String ssReqType;
		Bundle ssArgs;
		ProgressState ssState;

		SavedState(Parcelable superState) {
			super(superState);
		}

		private SavedState(Parcel in) {
			super(in);
			this.ssReqType = in.readString();
			this.ssArgs = in.readBundle();
			this.ssState = ProgressState.values()[in.readInt()];
		}

		@Override
		public void writeToParcel(Parcel out, int flags) {
			super.writeToParcel(out, flags);
			out.writeString(this.ssReqType);
			out.writeBundle(ssArgs);
			out.writeInt(this.ssState.ordinal());
		}

		// required field that makes Parcelables from a Parcel
		public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
			public SavedState createFromParcel(Parcel in) {
				return new SavedState(in);
			}

			public SavedState[] newArray(int size) {
				return new SavedState[size];
			}
		};
	}

	private final Runnable tooLongAction = new Runnable() {
		@Override
		public void run() {
			setState(ProgressState.LOADING_TOO_LONG);
		}
	};

	public RetriableProgressBar(Context context) {
		super(context);
		init();
	}

	public RetriableProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);

		int id;
		id = attrs.getAttributeResourceValue(XMLNS, "text_loading", -1);
		if (id != -1)
			textLoading = context.getString(id);

		id = attrs.getAttributeResourceValue(XMLNS, "text_loading_too_long", -1);
		if (id != -1)
			textLoadingTooLong = context.getString(id);

		id = attrs.getAttributeResourceValue(XMLNS, "text_error", -1);
		if (id != -1)
			textError = context.getString(id);

		id = attrs.getAttributeResourceValue(XMLNS, "text_empty", -1);
		if (id != -1)
			textEmpty = context.getString(id);

		id = attrs.getAttributeResourceValue(XMLNS, "text_retry", -1);
		if (id != -1)
			textRetry = context.getString(id);

		delayMs = attrs.getAttributeIntValue(XMLNS, "retry_delay", TRY_AGAIN_DELAY_MILLIS);

		init();

	}

	private void init() {
		mHandler = new Handler();
		LayoutInflater inflater = LayoutInflater.from(getContext());
		addView(inflater.inflate(R.layout.inc_progress_text_button, null));

		progressBar = this.findViewById(R.id.progress_bar);
		loadingTextView = (TextView) this.findViewById(R.id.loading_text);
		tryAgainButton = (Button) this.findViewById(R.id.retry_button);
		tryAgainButton.setText(textRetry);
	}

	public void attachActions(String id, Bundle args) {
		this.reqType = id;
		this.args = args;
		setProgressRetryAction();
	}

	public int getCurrentLoaderId() {
		return loaderId;
	}

	public void startEventListening() {
		EventBus.getDefault().register(this);
	}

	public void stopEventListening() {
		EventBus.getDefault().unregister(this);
	}

	@Override
	public Parcelable onSaveInstanceState() {
		Parcelable superState = super.onSaveInstanceState();
		SavedState ss = new SavedState(superState);
		ss.ssReqType = this.reqType;
		ss.ssState = this.state;
		ss.ssArgs = this.args;
		return ss;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (!(state instanceof SavedState)) {
			super.onRestoreInstanceState(state);
			return;
		}

		SavedState ss = (SavedState) state;
		super.onRestoreInstanceState(ss.getSuperState());
		this.reqType = ss.ssReqType;
		this.args = ss.ssArgs;
		this.state = ss.ssState;
		setState(this.state);
	}

	public void setState(ProgressState state) {
		if (this.state != ProgressState.LOADING && state == ProgressState.LOADING_TOO_LONG)
			return;
		this.state = state;
		if (state != null)
			state.applyTo(this);
	}

	private void setProgressRetryAction() {
		tryAgainButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				LOGD(TAG, "tryAgainClick()");
				if (reqType != "") {
					if (getContext() instanceof FragmentActivity) {
						LoaderManager loaderManager = ((FragmentActivity) getContext()).getSupportLoaderManager();
						EventBus.getDefault().post(new RetryLoaderEvent(reqType, args, loaderManager));
					} else {
						LOGE(TAG, "Can't retry, unexpected Context");
					}
				}
			}
		});
	}

	public void onEvent(OnLoaderCreatedEvent event) {
		LOGD(TAG, "OnLoaderCreatedEvent(" + event.getId() + ")");
		if (event.getId() == reqType.hashCode()) {
			LOGD(TAG, "Progress starting for reqType " + reqType);
			setState(ProgressState.LOADING);
			mHandler.postDelayed(tooLongAction, delayMs);
		}
	}

	public void onEvent(OnLoaderFinishedEvent event) {
		LOGD(TAG, "OnLoaderFinishedEvent(" + event.getId() + ")");
		if (event.getId() == reqType.hashCode()) {
			LOGD(TAG, "Progress finished for reqType " + reqType);
			setState(ProgressState.SUCCESS);
		}
	}
}
