package com.megatron.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class ImageViewStretched extends ImageView {

	private boolean isDrawChevron;
	private int width;
	private int height;
	private ImageView chevron;
	private int maxHeight;

	public ImageViewStretched(Context context) {
		super(context);
	}

	public ImageViewStretched(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ImageViewStretched(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public void setImageDrawable(Drawable drawable) {
		try {
			Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

			View parent = (View) getParent();
			width = parent.getWidth();
			height = width * drawable.getIntrinsicHeight() / drawable.getIntrinsicWidth();
			if (maxHeight > 0 && height > maxHeight) {
				height = maxHeight;
				width = height * drawable.getIntrinsicWidth() / drawable.getIntrinsicHeight();
			}

			if (isDrawChevron && width > chevron.getWidth()) {
				chevron.setVisibility(View.VISIBLE);
				chevron.setPadding(0, height - chevron.getDrawable().getIntrinsicHeight(), 0, 0);
			}
			super.setImageDrawable(drawable);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		try {
			setMeasuredDimension(width, height);
		} catch (Exception e) {
			super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		}
	}

	public void setChevron(View chevron) {
		this.isDrawChevron = true;
		this.chevron = (ImageView) chevron;
	}

	public void setMaxH(int maxHeight) {
		this.maxHeight = maxHeight;
	}
}
