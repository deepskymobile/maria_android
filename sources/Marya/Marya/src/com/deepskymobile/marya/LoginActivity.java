package com.deepskymobile.marya;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.deepskymobile.marya.model.Auth;
import com.deepskymobile.marya.model.Error;
import com.deepskymobile.marya.model.LoginReqData;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.net.RequestsManager;
import com.deepskymobile.marya.net.core.AbstractHttpLoader;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.UIUtils;
import com.google.gson.Gson;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * Login activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class LoginActivity extends FragmentActivity {

	private Button mBtnRegister;
	private Button mBtnRestorePassword;
	private EditText mEtUsername;
	private EditText mEtPassword;
	private CheckBox mCbRememberPassword;
	private Button mBtnLogin;

	private RetriableProgressBar progressPane;
	private ViewSwitcher switcher;
	private Button mBtnSend;
	private Button mBtnLogout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		mBtnRegister = (Button) findViewById(R.id.btn_register);
		mBtnRestorePassword = (Button) findViewById(R.id.btn_restore_password);
		mEtUsername = (EditText) findViewById(R.id.et_login_username);
		mEtPassword = (EditText) findViewById(R.id.et_login_password);
		mCbRememberPassword = (CheckBox) findViewById(R.id.cb_remember_password);
		mBtnLogin = (Button) findViewById(R.id.btn_login);
		mEtPassword = (EditText) findViewById(R.id.et_login_password);
		mEtUsername = (EditText) findViewById(R.id.et_login_username);
		mBtnSend = (Button) findViewById(R.id.btn_send);
		mBtnLogout = (Button) findViewById(R.id.btn_logout);

		mBtnLogin.setOnClickListener(oclListener);
		mBtnRegister.setOnClickListener(oclListener);
		mBtnRestorePassword.setOnClickListener(oclListener);
		mBtnLogout.setOnClickListener(oclListener);
		mBtnSend.setOnClickListener(oclListener);

		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		progressPane.setVisibility(View.INVISIBLE);
		switcher = (ViewSwitcher) findViewById(R.id.switcher);
	}

	@Override
	public void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
		progressPane.startEventListening();
		if (AbstractHttpLoader.hasCookies()) {
			switcher.setDisplayedChild(1);
		} else {
			switcher.setDisplayedChild(0);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_login:
				LoginReqData loginReqData = new LoginReqData();
				loginReqData.login = UIUtils.textOrError(mEtUsername, R.string.err_login_empty);
				loginReqData.password = UIUtils.textOrError(mEtPassword, R.string.err_password_empty);
				if (loginReqData.login == null || loginReqData.password == null) {
					return;
				}
				Gson gson = new Gson();
				String json = gson.toJson(loginReqData);
				Bundle args = new Bundle();
				args.putString(RequestsManager.EXTRA_BODY, json);
				progressPane.setVisibility(View.VISIBLE);
				findViewById(R.id.subcontent).setVisibility(View.GONE);
				UIUtils.startNewLoader(Consts.REQ_LOGIN, args, progressPane, getSupportLoaderManager());
				break;
			case R.id.btn_logout:
				switcher.setDisplayedChild(0);
				AbstractHttpLoader.removeCookies(getApplicationContext());
				break;
			case R.id.btn_send:
				Intent i = new Intent(LoginActivity.this, FeedbackformActivity.class);
				startActivity(i);
				break;
			case R.id.btn_register:
				// Open activity for registering new user
				Intent registerIntent = new Intent(getApplicationContext(), RegisterActivity.class);
				startActivity(registerIntent);
				break;
			case R.id.btn_restore_password:
				// Open activity for restoring user password
				Intent restorePasswordIntent = new Intent(getApplicationContext(), RestorePasswordActivity.class);
				startActivity(restorePasswordIntent);
				break;
			}
		}
	};

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_LOGIN.hashCode()) {
			findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
			progressPane.setVisibility(View.INVISIBLE);
			Response<Auth> data = (Response<Auth>) event.getData();
			Error errData = data.getError();
			boolean error = Response.isErrorResponse(data);
			switcher.setDisplayedChild(error ? 0 : 1);
			if (!error) {
				if (mCbRememberPassword.isChecked()) {
					AbstractHttpLoader.saveCookies(getApplicationContext());
				}
				Intent i = new Intent(this, FeedbackformActivity.class);
				startActivity(i);
			} else if (errData != null) {
				Toast.makeText(this, R.string.err_login_password, Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		}
	}
}
