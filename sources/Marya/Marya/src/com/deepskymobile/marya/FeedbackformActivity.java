package com.deepskymobile.marya;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.deepskymobile.marya.model.Error;
import com.deepskymobile.marya.model.FeedbackReqData;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.model.SimpleResp;
import com.deepskymobile.marya.net.RequestsManager;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.UIUtils;
import com.google.gson.Gson;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * Feedback form activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class FeedbackformActivity extends FragmentActivity {

	private EditText mEtFeedbackText;
	private Button mBtnSendFeedback;

	private RetriableProgressBar progressPane;

	private ViewSwitcher switcher;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feedbackform);

		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		progressPane.setVisibility(View.INVISIBLE);
		switcher = (ViewSwitcher) findViewById(R.id.switcher);

		mEtFeedbackText = (EditText) findViewById(R.id.et_feedback_text);
		mBtnSendFeedback = (Button) findViewById(R.id.btn_send_feedback);

		mBtnSendFeedback.setOnClickListener(oclListener);
	}

	@Override
	public void onResume() {
		super.onResume();
		EventBus.getDefault().register(this);
		progressPane.startEventListening();
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
	}

	OnClickListener oclListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_send_feedback:
				FeedbackReqData dt = new FeedbackReqData();
				if ((dt.text = UIUtils.textOrError(mEtFeedbackText, R.string.err_empty_text)) == null)
					return;
				switcher.setDisplayedChild(1);
				Bundle args = new Bundle();
				args.putString(RequestsManager.EXTRA_BODY, new Gson().toJson(dt));
				progressPane.setVisibility(View.VISIBLE);
				UIUtils.startNewLoader(Consts.REQ_FEEDBACK, args, progressPane, getSupportLoaderManager());
				break;
			}
		}
	};

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_FEEDBACK.hashCode()) {
			switcher.setDisplayedChild(0);
			progressPane.setVisibility(View.INVISIBLE);
			Response<SimpleResp> data = (Response<SimpleResp>) event.getData();
			Error errData = data.getError();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				Toast.makeText(this, R.string.feedback_ok, Toast.LENGTH_SHORT).show();
				finish();
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_SHORT).show();
			}
		}
	}
}
