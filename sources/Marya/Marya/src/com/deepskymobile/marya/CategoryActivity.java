package com.deepskymobile.marya;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.deepskymobile.marya.model.Categories;
import com.deepskymobile.marya.model.Category;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.ImageFetcher;
import com.deepskymobile.marya.util.UIUtils;
import com.megatron.widget.RetriableProgressBar;
import com.megatron.widget.RetriableProgressBar.ProgressState;

import de.greenrobot.event.EventBus;

/**
 * Activity for displaying categories
 * 
 * @author Fedor Kazakov
 * 
 */
public class CategoryActivity extends FragmentActivity {
	// ListView with category buttons
	private ListView listView;

	// Caterory of downloadable subcategory
	private int catId;
	private static final int DEFAULT_CATEGORY_ID = -1;
	private static final int FAV_CATEGORY_ID = -2;

	private RetriableProgressBar progressPane;

	private ImageFetcher mImageFetcher;

	private CategoryAdapter mAdapter;

	private String title;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_category);
		if (savedInstanceState != null && savedInstanceState.containsKey(Consts.KEY_PARAM_ID)) {
			catId = savedInstanceState.getInt(Consts.KEY_PARAM_ID);
		} else {
			catId = getIntent().getIntExtra(Consts.KEY_PARAM_ID, DEFAULT_CATEGORY_ID);
		}

		title = getIntent().getStringExtra(Consts.KEY_PARAM_TITLE);
		if (title != null) {
			((TextView) findViewById(R.id.tv_header)).setText(title);
		}

		listView = (ListView) findViewById(android.R.id.list);
		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);

		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);

	}

	@Override
	public void onResume() {
		super.onResume();
		progressPane.startEventListening();
		EventBus.getDefault().register(this);
		Bundle args = new Bundle();
		if (catId != DEFAULT_CATEGORY_ID)
			args.putInt(Consts.KEY_PARAM_ID, catId);
		if (mAdapter == null)
			UIUtils.startNewLoader(Consts.REQ_CATEGORY, args, progressPane, getSupportLoaderManager());
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
		mImageFetcher.flushCache();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_CATEGORY.hashCode()) {
			Response<Categories> data = (Response<Categories>) event.getData();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				fillContent(data.getData());
			} else {
				progressPane.setState(ProgressState.ERROR);
			}
		}
	}

	/**
	 * Creates list items
	 * 
	 * @param categories
	 * 
	 */
	public void fillContent(Categories categories) {
		if (categories.category == null || categories.category.length == 0) {
			progressPane.setState(ProgressState.EMPTY);
			return;
		}
		if (catId == DEFAULT_CATEGORY_ID) {
			Category[] arr = new Category[categories.category.length + 1];
			System.arraycopy(categories.category, 0, arr, 0, categories.category.length);
			Category favCat = new Category();
			favCat.id = FAV_CATEGORY_ID;
			favCat.name = getString(R.string.textview_header_favorites);
			arr[arr.length - 1] = favCat;
			mAdapter = new CategoryAdapter(this, mImageFetcher, arr);
		} else {
			mAdapter = new CategoryAdapter(this, mImageFetcher, categories.category, true);
		}

		// Set adapter
		listView.setAdapter(mAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (catId == DEFAULT_CATEGORY_ID) {
					Category item = mAdapter.getItem(position);
					Intent i;
					if (item.id == FAV_CATEGORY_ID) {
						i = new Intent(CategoryActivity.this, FavoritesActivity.class);
					} else {
						i = new Intent(CategoryActivity.this, CategoryActivity.class);
						i.putExtra(Consts.KEY_PARAM_ID, item.id);
						i.putExtra(Consts.KEY_PARAM_TITLE, item.name);
					}
					startActivity(i);
				} else {
					Intent i = new Intent(CategoryActivity.this, ProductListActivity.class);
					Category item = mAdapter.getItem(position);
					i.putExtra(Consts.KEY_PARAM_ID, item.id);
					i.putExtra(Consts.KEY_SUBCAT_NAME, item.name);
					i.putExtra(Consts.KEY_CAT_NAME, title);
					i.putExtra(Consts.KEY_PARAM_TITLE, item.name);
					startActivity(i);
				}
			}
		});
		findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
		findViewById(android.R.id.empty).setVisibility(View.GONE);
	}

}
