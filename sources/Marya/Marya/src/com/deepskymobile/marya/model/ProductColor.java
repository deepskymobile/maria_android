package com.deepskymobile.marya.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.deepskymobile.marya.util.UIUtils;

public class ProductColor implements Parcelable {
	public int id;
	public String name;
	private String image;
	private String big_image;
	private String colored_image;

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Parcelable.Creator<ProductColor> CREATOR = new Parcelable.Creator<ProductColor>() {

		@Override
		public ProductColor createFromParcel(Parcel source) {
			ProductColor result = new ProductColor();
			result.readFromParcel(source);
			return result;
		}

		@Override
		public ProductColor[] newArray(int size) {
			return new ProductColor[size];
		}
	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(image);
		dest.writeString(big_image);
		dest.writeString(colored_image);
	}

	private void readFromParcel(Parcel source) {
		id = source.readInt();
		name = source.readString();
		image = source.readString();
		big_image = source.readString();
		colored_image = source.readString();
	}

	public String getBestImage() {
		String result = null;
		
		if (UIUtils.isValidImageUrl(big_image))
			result = big_image;
		/*else if (UIUtils.isValidImageUrl(colored_image))
			result = colored_image;*/
		else if (UIUtils.isValidImageUrl(image))
			result = image;
		
		if (result != null && result.startsWith("/")) {
			result = "http://www.marya.ru" + result; 
		}
		
		return result;
	}
}
