package com.deepskymobile.marya.model;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {
	public int id;
	public String name;
	public String catName;
	public String subcatName;
	public String description;
	public String big_image;
	public String thumbnail_url;
	public int default_color;
	public InnerColor[] colors;
	public String[] images;
	public int product_count;

	@Override
	public int describeContents() {
		return 0;
	}

	public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {

		@Override
		public Product createFromParcel(Parcel source) {
			Product result = new Product();
			result.readFromParcel(source);
			return result;
		}

		@Override
		public Product[] newArray(int size) {
			return new Product[size];
		}
	};

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(id);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeInt(default_color);
		dest.writeString(thumbnail_url);
		dest.writeString(big_image);
		int len = colors == null ? 0 : colors.length;
		dest.writeInt(len);
		for (int i = 0; i < len; i++) {
			dest.writeParcelable(colors[i], 0);
		}
		len = images == null ? 0 : images.length;
		dest.writeInt(len);
		for (int i = 0; i < len; i++) {
			dest.writeString(images[i]);

		}
		dest.writeInt(product_count);
	}

	protected void readFromParcel(Parcel dest) {
		ClassLoader loader = getClass().getClassLoader();
		id = dest.readInt();
		name = dest.readString();
		description = dest.readString();
		default_color = dest.readInt();
		thumbnail_url = dest.readString();
		big_image = dest.readString();
		int len = dest.readInt();
		colors = new InnerColor[len];
		for (int i = 0; i < len; i++) {
			colors[i] = dest.readParcelable(loader);
		}
		len = dest.readInt();
		images = new String[len];
		for (int i = 0; i < len; i++) {
			images[i] = dest.readString();

		}
		product_count = dest.readInt();
	}

	public ProductColor getColor(int id) {
		if (colors == null)
			return null;
		for (int i = 0; i < colors.length; i++) {
			ProductColor color = colors[i].product_color;
			if (color.id == id)
				return color;
		}
		return null;
	}

	public ProductColor[] getColors() {
		ArrayList<ProductColor> list = new ArrayList<ProductColor>();
		if (colors != null) {
			for (int i = 0; i < colors.length; i++) {
				ProductColor color = colors[i].product_color;
				if (color != null && color.getBestImage() != null)
					list.add(color);
			}
		}
		return list.toArray(new ProductColor[list.size()]);
	}

	@Override
	public boolean equals(Object o) {
		return o != null && o instanceof Product && id == ((Product) o).id;
	}
}
