package com.deepskymobile.marya.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Shop implements Parcelable {
	public int shopid;
	public int sortid;
	public String name;
	public String description;
	public String city;
	public String mall;
	public String address;
	public String metro;
	public String email;
	public String phone;
	public String worktime;
	public double latitude;
	public double longitude;
	public String image;

	public static final Parcelable.Creator<Shop> CREATOR = new Parcelable.Creator<Shop>() {

		@Override
		public Shop createFromParcel(Parcel source) {
			Shop result = new Shop();
			result.shopid = source.readInt();
			result.sortid = source.readInt();
			result.name = source.readString();
			result.description = source.readString();
			result.city = source.readString();
			result.mall = source.readString();
			result.address = source.readString();
			result.metro = source.readString();
			result.email = source.readString();
			result.phone = source.readString();
			result.worktime = source.readString();
			result.latitude = source.readDouble();
			result.longitude = source.readDouble();
			result.image = source.readString();
			return result;
		}

		@Override
		public Shop[] newArray(int size) {
			return new Shop[size];
		}
	};

	@Override
	public int describeContents() {
		return 0;
	}

	public String getSnippet() {
		return city + ", " + address;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeInt(shopid);
		dest.writeInt(sortid);
		dest.writeString(name);
		dest.writeString(description);
		dest.writeString(city);
		dest.writeString(mall);
		dest.writeString(address);
		dest.writeString(metro);
		dest.writeString(email);
		dest.writeString(phone);
		dest.writeString(worktime);
		dest.writeDouble(latitude);
		dest.writeDouble(longitude);
		dest.writeString(image);
	}

}
