package com.deepskymobile.marya;

import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.deepskymobile.marya.model.Error;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.model.Shop;
import com.deepskymobile.marya.model.Shops;
import com.deepskymobile.marya.net.RequestsManager;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.UIUtils;
import com.megatron.widget.RetriableProgressBar;

import de.greenrobot.event.EventBus;

/**
 * Activity for displaying shop list
 * 
 * @author Fedor Kazakov
 * 
 */
public class ShopsListActivity extends FragmentActivity implements OnItemClickListener {
	private ListView lvShops;
	private Shop[] shops;
	private Location location;
	private RetriableProgressBar progressPane;
	private ShopsAdapter adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_shops_list);
		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);
		lvShops = (ListView) findViewById(android.R.id.list);
		Parcelable[] parcelableArray = getIntent().getParcelableArrayExtra("shops");
		if (getIntent().hasExtra("location"))
			location = (Location) getIntent().getParcelableExtra("location");
		if (parcelableArray != null) {
			shops = new Shop[parcelableArray.length];
			for (int i = 0; i < parcelableArray.length; ++i) {
				shops[i] = (Shop) parcelableArray[i];
			}
			setAdapter();
		} else {
			Bundle args = new Bundle();
			args.putBoolean(RequestsManager.EXTRA_CACHE_FIRST, true);
			UIUtils.startNewLoader(Consts.REQ_SHOPS, args, progressPane, getSupportLoaderManager());
		}
	}

	@Override
	protected void onResume() {
		super.onResume();
		progressPane.startEventListening();
		EventBus.getDefault().register(this);
	}

	@Override
	protected void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
	}

	private void setAdapter() {
		findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
		progressPane.setVisibility(View.GONE);
		adapter = new ShopsAdapter(this, shops, location);
		lvShops.setAdapter(adapter);
		lvShops.setOnItemClickListener(this);
		adapter.sortByDistance();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_SHOPS.hashCode()) {
			Response<Shops> data = (Response<Shops>) event.getData();
			Error errData = data.getError();
			boolean error = data == null || errData != null;
			if (!error) {
				shops = data.getData().shops;
				setAdapter();
			} else if (errData != null) {
				Toast.makeText(this, errData.error.replace('|', '\n'), Toast.LENGTH_LONG).show();
			} else {
				Toast.makeText(this, R.string.err_unknown, Toast.LENGTH_LONG).show();
			}
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Shop item = adapter.getItem(position);
		Intent i = new Intent(this, ShopDescriptionActivity.class);
		i.putExtra("shop", item);
		startActivity(i);
	}
}
