package com.deepskymobile.marya;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

import com.deepskymobile.marya.model.Product;
import com.deepskymobile.marya.model.Products;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.ImageFetcher;
import com.deepskymobile.marya.util.UIUtils;
import com.megatron.widget.RetriableProgressBar;
import com.megatron.widget.RetriableProgressBar.ProgressState;

import de.greenrobot.event.EventBus;

/**
 * Product list activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class ProductListActivity extends FragmentActivity implements OnItemClickListener {

	private RetriableProgressBar progressPane;
	private ImageFetcher mImageFetcher;
	private int catId;
	private Product[] products;
	private ProductAdapter adapter;
	private GridView gridView;
	private String catName;
	private String subcatName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product_list);
		gridView = (GridView) findViewById(R.id.gv_products);
		catId = getIntent().getIntExtra(Consts.KEY_PARAM_ID, 0);
		catName = getIntent().getStringExtra(Consts.KEY_CAT_NAME);
		subcatName = getIntent().getStringExtra(Consts.KEY_SUBCAT_NAME);
		String title = getIntent().getStringExtra(Consts.KEY_PARAM_TITLE);
		((TextView) findViewById(R.id.tv_header)).setText(title);
		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);

		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);
	}

	@Override
	public void onResume() {
		super.onResume();
		progressPane.startEventListening();
		EventBus.getDefault().register(this);
		if (products == null) {
			Bundle args = new Bundle();
			args.putInt(Consts.KEY_CAT_ID, catId);
			args.putInt(Consts.KEY_COUNT, 1000);
			UIUtils.startNewLoader(Consts.REQ_PRODUCT, args, progressPane, getSupportLoaderManager());
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		EventBus.getDefault().unregister(this);
		mImageFetcher.flushCache();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_PRODUCT.hashCode()) {
			Response<Products> data = (Response<Products>) event.getData();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				if (data.getData().products_count == 0) {
					progressPane.setState(ProgressState.EMPTY);
				} else {
					findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
					progressPane.setVisibility(View.GONE);
					fillContent(data.getData().products);
				}
			} else {
				progressPane.setState(ProgressState.ERROR);
			}
		}
	}

	private void fillContent(Product[] products) {
		this.products = products;
		adapter = new ProductAdapter(this, mImageFetcher, products);
		gridView.setAdapter(adapter);
		gridView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		Product item = adapter.getItem(position);
		Intent i = new Intent(this, ProductActivity.class);
		i.putExtra(Consts.KEY_PARAM_ID, item.id);
		i.putExtra(Consts.KEY_CAT_NAME, catName);
		i.putExtra(Consts.KEY_SUBCAT_NAME, subcatName);
		startActivity(i);
	}
}
