package com.deepskymobile.marya;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.deepskymobile.marya.model.ProductColor;
import com.deepskymobile.marya.util.ImageFetcher;

public class GalleryAdapter extends ArrayAdapter<ProductColor> {

	private LayoutInflater inflater;
	private ImageFetcher mImageFetcher;

	public GalleryAdapter(Context context, ImageFetcher imageFetcher, ProductColor[] colors) {
		super(context, R.layout.gallery_item_photo, colors);
		inflater = LayoutInflater.from(getContext());
		mImageFetcher = imageFetcher;
	}

	static class ViewHolder {
		ImageView img;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.gallery_item_photo, parent, false);

			holder = new ViewHolder();
			holder.img = (ImageView) convertView.findViewById(R.id.image);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

		ProductColor item = getItem(position);

		String bestImage = item.getBestImage();
		mImageFetcher.loadThumbnailImage(bestImage, holder.img, R.drawable.blank);

		return convertView;
	}
}
