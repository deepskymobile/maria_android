package com.deepskymobile.marya;

import java.util.List;

import android.content.Context;
import android.graphics.Rect;
import android.view.LayoutInflater;
import android.view.TouchDelegate;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deepskymobile.marya.model.Product;
import com.deepskymobile.marya.util.ImageFetcher;

public class FavAdapter extends ArrayAdapter<Product> {

	private LayoutInflater inflater;
	private OnClickListener listener;
	private ImageFetcher mImageFetcher;

	public FavAdapter(Context context, ImageFetcher imageFetcher, List<Product> list, OnClickListener listener) {
		super(context, R.layout.fav_item, list);
		inflater = LayoutInflater.from(getContext());
		mImageFetcher = imageFetcher;
		this.listener = listener;
	}

	static class ViewHolder {
		TextView name;
		TextView category;
		TextView subcategory;
		ImageView img;
		ImageView remove;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.fav_item, parent, false);

			holder = new ViewHolder();
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.category = (TextView) convertView.findViewById(R.id.category);
			holder.subcategory = (TextView) convertView.findViewById(R.id.subcategory);
			holder.img = (ImageView) convertView.findViewById(R.id.image);
			holder.remove = (ImageView) convertView.findViewById(R.id.remove);
			convertView.setTag(holder);
		}

		holder = (ViewHolder) convertView.getTag();

		Product item = getItem(position);

		holder.name.setText(item.name);
		holder.category.setText(item.catName);
		holder.subcategory.setText(item.subcatName);

		if (item.thumbnail_url != null && item.thumbnail_url.startsWith("http")) {
			mImageFetcher.loadThumbnailImage(item.thumbnail_url, holder.img, R.drawable.blank);
			holder.img.setVisibility(View.VISIBLE);
		} else {
			holder.img.setVisibility(View.INVISIBLE);
		}

		holder.remove.setTag(item);
		holder.remove.setOnClickListener(listener);
		final View pv = (View) holder.remove.getParent();
		final View dv = holder.remove;
		pv.post(new Runnable() {
			public void run() {
				final Rect r = new Rect();
				dv.getHitRect(r);
				r.top -= 10;
				r.bottom += 10;
				r.left -= 10;
				r.right += 10;
				pv.setTouchDelegate(new TouchDelegate(r, dv));
			}
		});

		return convertView;
	}
}
