package com.deepskymobile.marya;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.deepskymobile.marya.model.Auth;
import com.deepskymobile.marya.model.Categories;
import com.deepskymobile.marya.model.ProductCount;
import com.deepskymobile.marya.model.Products;
import com.deepskymobile.marya.model.Shops;
import com.deepskymobile.marya.model.ShopsVersion;
import com.deepskymobile.marya.model.SimpleResp;

public class Consts {

	public static final String HOST = "http://www.marya.ru/";
	public static final long SPLASH_DELAY = 2000l;
	public static final String REQ_LOGIN = "login.json";
	public static final String REQ_REGISTER = "register.json";
	public static final String REQ_LOGOUT = "logout.json";
	public static final String REQ_REMIND = "remind.json";
	public static final String REQ_CATEGORY = "category.json";
	public static final String REQ_PRODUCT = "product.json";
	public static final String REQ_PRODUCTION_COUNT = "productcount.json";
	public static final String REQ_FEEDBACK = "feedback.json";
	public static final String REQ_SHOPS = "shops.json";
	public static final String REQ_SHOP_VERSION = "shopsversion.json";

	public static final Set<String> canCacheReqType = new HashSet<String>();
	public static final Map<String, Class<?>> responseClasses = new HashMap<String, Class<?>>();
	public static final String KEY_PARAM_ID = "id";
	public static final String KEY_CAT_ID = "catid";
	public static final String KEY_CAT_NAME = "catname";
	public static final String KEY_SUBCAT_NAME = "subcatname";
	public static final String KEY_PARAM_TITLE = "title";
	public static final String KEY_PARAM_LOGIN = "login";
	public static final String KEY_PARAM_PASSWORD = "password";
	
	public static final String SETTINGS_NAME = "marya.settings";
	public static final String KEY_COUNT = "count";

	static {
		responseClasses.put(REQ_LOGIN, Auth.class);
		responseClasses.put(REQ_REGISTER, Auth.class);
		responseClasses.put(REQ_LOGOUT, SimpleResp.class);
		responseClasses.put(REQ_REMIND, SimpleResp.class);
		responseClasses.put(REQ_CATEGORY, Categories.class);
		responseClasses.put(REQ_PRODUCT, Products.class);
		responseClasses.put(REQ_PRODUCTION_COUNT, ProductCount.class);
		responseClasses.put(REQ_FEEDBACK, SimpleResp.class);
		responseClasses.put(REQ_SHOPS, Shops.class);
		responseClasses.put(REQ_SHOP_VERSION, ShopsVersion.class);

		canCacheReqType.add(REQ_SHOPS);
	}
}