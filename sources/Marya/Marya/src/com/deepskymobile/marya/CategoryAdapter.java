package com.deepskymobile.marya;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.deepskymobile.marya.model.Category;
import com.deepskymobile.marya.util.ImageFetcher;

public class CategoryAdapter extends ArrayAdapter<Category> {

	private LayoutInflater inflater;
	private ImageFetcher mImageFetcher;
	private boolean noImage;

	public CategoryAdapter(Context context, ImageFetcher imageFetcher, Category[] list) {
		this(context, imageFetcher, list, false);
	}

	public CategoryAdapter(Context context, ImageFetcher imageFetcher, Category[] list, boolean noImage) {
		super(context, R.layout.category_item, list);
		inflater = LayoutInflater.from(getContext());
		mImageFetcher = imageFetcher;
		this.noImage = noImage;
	}

	static class ViewHolder {
		TextView title;
		ImageView img;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.category_item, parent, false);

			ViewHolder holder1 = new ViewHolder();
			holder1.title = (TextView) convertView.findViewById(R.id.tvText);
			holder1.img = (ImageView) convertView.findViewById(R.id.ivImg);
			convertView.setTag(holder1);
		}
		holder = (ViewHolder) convertView.getTag();

		Category item = getItem(position);

		holder.title.setText(item.name);

		if (noImage) {
			holder.img.setVisibility(View.GONE);
		} else {
			if (item.image != null && item.image.startsWith("http")) {
				mImageFetcher.loadThumbnailImage(item.image, holder.img, R.drawable.blank);
				holder.img.setVisibility(View.VISIBLE);
			} else {
				holder.img.setVisibility(View.INVISIBLE);
			}
		}

		return convertView;
	}
}
