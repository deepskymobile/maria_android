package com.deepskymobile.marya;

/**
 * Common constants
 * 
 * @author Fedor Kazakov
 * 
 */
public final class Constants {
	// Constants for handlers
	public static final int MSG_GETCATALOG = 1;
	public static final int MSG_GOTCATALOG = 2;
	public static final int MSG_GETSUBCATALOG = 3;
	public static final int MSG_GOTSUBCATALOG = 4;
	public static final int MSG_SENDREGINFO = 5;
	public static final int MSG_RECEIVEREGINFO = 6;
	public static final int MSG_SENDRESTOREPASSWORD = 7;
	public static final int MSG_RECEIVERESTOREPASSWORD = 8;
	public static final int MSG_GETSHOPS = 9;
	public static final int MSG_RECEIVESHOPS = 10;
	public static final int MSG_LOGINUSER = 11;
	public static final int MSG_USERLOGGEDIN = 12;
	public static final int MSG_SENDFEEDBACK = 13;
	public static final int MSG_FEEDBACKSENT = 14;

	// Constants for listview buttons
	public static final String ATTRIBUTE_NAME_TEXT = "text";
	public static final String ATTRIBUTE_NAME_IMAGE = "image";
	public static final String ATTRIBUTE_NAME_ARROW = "arrow";

	// Constant for detecting internet status
	public static final String NO_INTERNET = "notconnected";

	// Constants, used by application
	public static final String SITE_URL = "http://www.marya.ru/";
	public static final String CALL_NUMBER = "88001003131";

	// Application tag for debugger
	public static final String LOG_APPNAME = "Marya";
}
