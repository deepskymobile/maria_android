package com.deepskymobile.marya.net.core;

/**
 * Interface for cancel event listeners
 * 
 * @author Fedor Kazakov
 */
public interface OnCancelListener {

	public void onCancel(Object obj);
}
