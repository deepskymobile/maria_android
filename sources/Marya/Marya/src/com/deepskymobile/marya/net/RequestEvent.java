package com.deepskymobile.marya.net;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;

public class RequestEvent {

	public String reqType;

	public Bundle args;

	public LoaderManager loaderManager;

	public RequestEvent(LoaderManager loaderManager, String reqType, Bundle args) {
		this.loaderManager = loaderManager;
		this.reqType = reqType;
		this.args = args;
	}

}
