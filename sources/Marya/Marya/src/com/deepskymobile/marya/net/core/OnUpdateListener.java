package com.deepskymobile.marya.net.core;

/**
 * Interface for update event listeners
 * 
 * @author Fedor Kazakov
 */
public interface OnUpdateListener {

	public void onUpdate(Object obj);
}
