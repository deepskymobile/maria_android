package com.deepskymobile.marya;

import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.TextView;

public class AboutProgramActivity extends FragmentActivity {

	private String version;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aboutprogram);
		TextView tv = (TextView) findViewById(R.id.tvText2);
		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			version = pInfo.versionName;
			tv.setText(getString(R.string.about_2, version));
		} catch (NameNotFoundException e) {
			tv.setText(getString(R.string.about_2, ""));
		}
	}
}
