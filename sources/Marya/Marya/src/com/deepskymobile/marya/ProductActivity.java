package com.deepskymobile.marya;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.text.Html;
import android.view.Display;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.deepskymobile.marya.favorites.FavoritesManagerImpl;
import com.deepskymobile.marya.model.InnerColor;
import com.deepskymobile.marya.model.Product;
import com.deepskymobile.marya.model.Products;
import com.deepskymobile.marya.model.Response;
import com.deepskymobile.marya.net.core.events.OnLoaderFinishedEvent;
import com.deepskymobile.marya.util.ImageFetcher;
import com.deepskymobile.marya.util.UIUtils;
import com.megatron.widget.ImageViewStretched;
import com.megatron.widget.RetriableProgressBar;
import com.megatron.widget.RetriableProgressBar.ProgressState;

import de.greenrobot.event.EventBus;

/**
 * Product activity
 * 
 * @author Fedor Kazakov
 * 
 */
public class ProductActivity extends FragmentActivity {
	private ListView listView;
	private TextView mTvHeader;
	private ImageViewStretched mIvProdPhoto;
	private String mHeaderText;

	private RetriableProgressBar progressPane;

	private ImageFetcher mImageFetcher;
	private int mId;
	private Product product;
	private String catName;
	private String subcatName;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_product);

		listView = (ListView) findViewById(android.R.id.list);
		mTvHeader = (TextView) findViewById(R.id.tv_header);
		mIvProdPhoto = (ImageViewStretched) findViewById(R.id.iv_prodPhoto);
		mHeaderText = getIntent().getStringExtra(Consts.KEY_PARAM_TITLE);
		mId = getIntent().getIntExtra(Consts.KEY_PARAM_ID, 0);
		mTvHeader.setText(mHeaderText);
		catName = getIntent().getStringExtra(Consts.KEY_CAT_NAME);
		subcatName = getIntent().getStringExtra(Consts.KEY_SUBCAT_NAME);

		getLayoutInflater().inflate(R.layout.empty_container, (ViewGroup) findViewById(android.R.id.empty), true);
		progressPane = (RetriableProgressBar) findViewById(R.id.progress_pane);

		mImageFetcher = ImageFetcher.getImageFetcher(this);
		mImageFetcher.setImageFadeIn(false);
	}

	@Override
	public void onResume() {
		super.onResume();
		progressPane.startEventListening();
		EventBus.getDefault().register(this);
		Bundle args = new Bundle();
		args.putInt(Consts.KEY_PARAM_ID, mId);
		if (product == null)
			UIUtils.startNewLoader(Consts.REQ_PRODUCT, args, progressPane, getSupportLoaderManager());
	}

	@Override
	public void onPause() {
		super.onPause();
		progressPane.stopEventListening();
		mImageFetcher.flushCache();

		EventBus.getDefault().unregister(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		mImageFetcher.closeCache();
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private void onEvent(OnLoaderFinishedEvent event) {
		if (event.getId() == Consts.REQ_PRODUCT.hashCode()) {
			Response<Products> data = (Response<Products>) event.getData();
			boolean error = Response.isErrorResponse(data);
			if (!error) {
				fillContent(data.getData().products[0]);
			} else {
				progressPane.setState(ProgressState.ERROR);
			}
		}
	}

	/**
	 * Creates list items
	 * 
	 * @param product
	 * 
	 */
	public void fillContent(final Product product) {
		this.product = product;
		findViewById(R.id.subcontent).setVisibility(View.VISIBLE);
		View imgChevron = findViewById(R.id.che);
		mIvProdPhoto.setChevron(imgChevron);
		mIvProdPhoto.setMaxH(getScreenH() * 4 / 10);
		OnClickListener photoClickListener = new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent i = new Intent(ProductActivity.this, GalleryActivity.class);
				i.putExtra("product", product);
				startActivity(i);
			}
		};
		mIvProdPhoto.setOnClickListener(photoClickListener);
		imgChevron.setOnClickListener(photoClickListener);

		if (UIUtils.isValidImageUrl(product.big_image))
			mImageFetcher.loadThumbnailImage(product.big_image, mIvProdPhoto, R.drawable.product_image_stub);
		else if (UIUtils.isValidImageUrl(product.thumbnail_url))
			mImageFetcher.loadThumbnailImage(product.thumbnail_url, mIvProdPhoto, R.drawable.product_image_stub);
		mTvHeader.setText(product.name);
		// List items text
		String[] texts = { getResources().getString(R.string.list_description),
				getResources().getString(R.string.list_colors), getResources().getString(R.string.list_share),
				getResources().getString(R.string.list_tofavorites) };

		ArrayList<Map<String, Object>> data = new ArrayList<Map<String, Object>>(texts.length);
		Map<String, Object> m;
		for (int i = 0; i < texts.length; i++) {
			m = new HashMap<String, Object>();
			m.put(Constants.ATTRIBUTE_NAME_TEXT, texts[i]);
			data.add(m);
		}

		String[] from = { Constants.ATTRIBUTE_NAME_TEXT };
		int[] to = { R.id.tvText };

		// Creates adapter
		SimpleAdapter sAdapter = new SimpleAdapter(this, data, R.layout.product_options_item, from, to);

		listView.setAdapter(sAdapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				switch (position) {
				case 0:
					showDescription(product);
					break;
				case 1:
					colorDialog(product);
					break;
				case 2:
					share(product);
					break;
				case 3:
					FavoritesManagerImpl fav = FavoritesManagerImpl.getInstance(ProductActivity.this);
					if (fav.isAdded(product)) {
						Toast.makeText(ProductActivity.this, R.string.already_in_favorites, Toast.LENGTH_SHORT).show();
					} else {
						product.catName = catName;
						product.subcatName = subcatName;
						fav.add(product);
						Toast.makeText(ProductActivity.this, R.string.added_to_favorites, Toast.LENGTH_SHORT).show();
					}
					break;
				}
			}

		});
	}

	private int getScreenH() {
		Display display = getWindowManager().getDefaultDisplay();
		return display.getHeight();
	}

	protected void showDescription(Product product) {
		UIUtils.alertOk(this, product.name, Html.fromHtml(product.description));
	}

	protected void colorDialog(Product product) {
		final InnerColor[] icolors = product.colors;
		if (icolors == null || icolors.length < 2) {
			Toast.makeText(ProductActivity.this, R.string.too_few_colors, Toast.LENGTH_LONG).show();
			return;
		}

		Intent i = new Intent(ProductActivity.this, GalleryActivity.class);
		i.putExtra("product", product);
		i.putExtra("images", false);
		startActivity(i);
	}

	protected void share(Product product) {
		Intent share = new Intent(Intent.ACTION_SEND);
		// share.setType("image/*");
		share.putExtra(Intent.EXTRA_SUBJECT, product.name);
		// share.putExtra(Intent.EXTRA_TEXT, product.description);
		try {
			copyImageToPubliclyAccessibleFile();
			Uri uri = Uri.fromFile(getFileStreamPath("marya.png"));
			share.putExtra(Intent.EXTRA_STREAM, uri);
		} catch (Exception e) {
		}
		share.setType("text/plain");
		String URL = "http://www.marya.ru/catalog/" + product.id + "/";
		share.putExtra(Intent.EXTRA_TEXT, URL);

		startActivity(Intent.createChooser(share, getString(R.string.list_share)));

	}

	private static final String SHARED_FILE_NAME = "marya.png";

	private void copyImageToPubliclyAccessibleFile() {
		FileOutputStream outputStream = null;
		try {
			try {
				deleteFile(SHARED_FILE_NAME);
			} catch (Exception e) {
			}
			outputStream = openFileOutput(SHARED_FILE_NAME, Context.MODE_WORLD_READABLE | Context.MODE_APPEND);
			((BitmapDrawable) mIvProdPhoto.getDrawable()).getBitmap().compress(CompressFormat.JPEG, 75, outputStream);
			outputStream.flush();
		} catch (FileNotFoundException fnfe) {
		} catch (IOException e) {
		} finally {
			try {
				outputStream.close();
			} catch (IOException ioe) {
				/* ignore */
			}
		}
	}

}
