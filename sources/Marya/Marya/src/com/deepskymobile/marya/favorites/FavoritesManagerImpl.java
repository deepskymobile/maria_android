package com.deepskymobile.marya.favorites;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.deepskymobile.marya.model.Product;
import com.deepskymobile.marya.model.Products;
import com.google.gson.Gson;

public final class FavoritesManagerImpl implements FavoritesManager {

	private static final String FILENAME = "fav.dat";
	private static volatile FavoritesManagerImpl instance;
	private List<Product> list;
	private Context context;

	private FavoritesManagerImpl(Context context) {
		this.context = context;
		list = new ArrayList<Product>();
	}

	public static FavoritesManagerImpl getInstance(Context context) {
		if (instance == null) {
			instance = new FavoritesManagerImpl(context);
			instance.load();
		}
		return instance;
	}

	@Override
	public void add(Product cl) {
		list.add(cl);
		save();
	}

	@Override
	public void remove(Product cl) {
		synchronized (list) {
			list.remove(cl);
		}
		save();
	}

	@Override
	public boolean isAdded(Product cl) {
		boolean result;
		synchronized (list) {
			result = list.contains(cl);
		}
		return result;
	}

	@Override
	public List<Product> getAll() {
		List<Product> result;

		synchronized (list) {
			result = new ArrayList<Product>(list.size());
			for (Product ccm : list) {
				result.add(ccm);
			}
		}

		return result;
	}
	
	@Override
	public void clear() {
		synchronized (list) {
			list.clear();
			save();
		}
	}

	private void load() {
		try {
			FileInputStream fis;
			fis = context.openFileInput(FILENAME);
			Reader reader = new InputStreamReader(fis, "utf-8"); 
			StringBuilder buffer = new StringBuilder();
			try {
				char[] tmp = new char[1024];
				int l;
				while ((l = reader.read(tmp)) != -1) {
					buffer.append(tmp, 0, l);
				}
			} finally {
				reader.close();
			}

			Products products = new Gson().fromJson(buffer.toString(), Products.class);
			list = new ArrayList<Product>(products.products.length);
			for (Product product : products.products) {
				list.add(product);
			}
			reader.close();
		} catch (IOException e) {
		}
	}

	private void save() {
		try {
			FileOutputStream fos;
			fos = context.openFileOutput(FILENAME, Context.MODE_PRIVATE);
			Writer writer = new OutputStreamWriter(fos);
			Products pr = new Products(list);
			writer.write(new Gson().toJson(pr));
			writer.flush();
			writer.close();
		} catch (IOException e) {
		}
	}

	@Override
	public boolean isEmpty() {
		return list.isEmpty();
	}

}
