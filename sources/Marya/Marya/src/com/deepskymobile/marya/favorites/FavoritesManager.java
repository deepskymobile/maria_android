package com.deepskymobile.marya.favorites;

import java.util.List;

import com.deepskymobile.marya.model.Product;

public interface FavoritesManager {
	public void add(Product cl);

	public void remove(Product cl);

	public boolean isAdded(Product cl);

	public List<Product> getAll();

	public void clear();

	public boolean isEmpty();
}
