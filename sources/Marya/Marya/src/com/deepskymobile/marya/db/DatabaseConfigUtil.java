package com.deepskymobile.marya.db;

import com.deepskymobile.marya.net.core.Etag;
import com.deepskymobile.marya.net.core.HttpResponseCache;

import com.j256.ormlite.android.apptools.OrmLiteConfigUtil;

public class DatabaseConfigUtil extends OrmLiteConfigUtil {
	public static final Class<?>[] classes = new Class[] { Etag.class, HttpResponseCache.class };

	public static void main(String[] args) throws Exception {
		writeConfigFile("ormlite_config.txt", classes);
	}
}